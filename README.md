Petit Ecolo
========================

Ce repository contient le code du site PetitEcolo.fr, un site de petites annonces de produits et services respectueux de tout et de tous pour favoriser une consommation plus durable.

Développement
--------------
Cette application est développée avec Symfony 3.4. 


Licence
--------------

Le code est distribué sous lience [GNU AGPL-3.0](https://framagit.org/wyllyjon/petit-ecolo-open-source/blob/master/LICENSE.txt).