(function($) {
    "use strict";

    /* =========== Tooltip =============== */
    $('.help').tooltip();

    /* ======= Preloader ======= */
    setTimeout(function() {
        $('body').addClass('loaded');
    }, 3000);

    /* ======= Counter FunFacts ======= */
    // var timer = $('.timer');
    // if (timer.length) {
    //     timer.appear(function() {
    //         timer.countTo();
    //     });
    // }

    /* ======= List Grid Style Switcher ======= */
    // $('#list').on("click", function(event) {
    //     event.preventDefault();
    //     $(this).addClass('active');
    //     $('#grid').removeClass('active');
    //     $('#products .item').addClass('list-group-items');
    // });
    // $('#grid').on("click", function(event) {
    //     event.preventDefault();
    //     $(this).addClass('active');
    //     $('#list').removeClass('active');
    //     $('#products .item').removeClass('list-group-items');
    //     $('#products .item').addClass('grid-group-item');
    // });

    /* ======= Sticky Ads ======= */
    $('.leftbar-stick, .rightbar-stick').theiaStickySidebar({
        additionalMarginTop: 80
    });

    /* ======= Accordion Panels ======= */
    // $('.accordion-title a').on('click', function(event) {
    //     event.preventDefault();
    //     if ($(this).parents('li').hasClass('open')) {
    //         $(this).parents('li').removeClass('open').find('.accordion-content').slideUp(400);
    //     } else {
    //         $(this).parents('.accordion').find('.accordion-content').not($(this).parents('li').find('.accordion-content')).slideUp(400);
    //         $(this).parents('.accordion').find('> li').not($(this).parents('li')).removeClass('open');
    //         $(this).parents('li').addClass('open').find('.accordion-content').slideDown(400);
    //     }
    // });

    /* ======= Accordion Style 2 ======= */
    // $('#accordion').on('shown.bs.collapse', function() {
    //     var offset = $('.panel.panel-default > .panel-collapse.in').offset();
    //     if (offset) {
    //         $('html,body').animate({
    //             scrollTop: $('.panel-title a').offset().top - 20
    //         }, 500);
    //     }
    // });

    /* ======= Jquery CheckBoxes ======= */
    // $('.skin-minimal .list li input').iCheck({
    //     checkboxClass: 'icheckbox_minimal',
    //     radioClass: 'iradio_minimal',
    //     increaseArea: '20%' // optional
    // });

    /* ======= Jquery Select Dropdowns ======= */
    $("select:not(.selectMonnaie)").select2({
        placeholder: "Selectionnez une option",
        allowClear: true,
        width: '100%'
    });
    $("select.selectMonnaie").select2({
        placeholder: "Selectionnez une monnaie",
        allowClear: true,
        width: '100%',
        minimumResultsForSearch: Infinity,
        templateResult: selectMonnaieTemplate
    });

    function selectMonnaieTemplate(monnaie)
    {
        if (monnaie.id == "g1")
        {
            return $("<span>"+monnaie.text+"</span><span class=\"help\"><i class=\"fa fa-question-circle\" title=\""+messageAideJune+"\"></i></span>") ;
        }
        else if (monnaie.id == "dug1")
        {
            return $("<span>"+monnaie.text+"</span><span class=\"help\"><i class=\"fa fa-question-circle\" title=\""+messageAideDUJune+"\"></i></span>") ;
        }
        return monnaie.text;
    }


    /*==========  Single Page SLider With Thumb ==========*/
    $('#carousels').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 110,
        itemMargin: 18,
        asNavFor: '.single-page-slider'
    });
    $('.single-page-slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: true,
        sync: "#carousel"
    });


    /*==========  Back To Top  ==========*/
    	var offset = 300,
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');
		//hide or show the "back to top" link
		$(window).scroll(function() {
			($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
			if ($(this).scrollTop() > offset_opacity) {
				$back_to_top.addClass('cd-fade-out');
			}
		});
    	//smooth scroll to top
		$back_to_top.on('click', function(event) {
	
			event.preventDefault();
			$('body,html').animate({
				scrollTop: 0,
			}, scroll_top_duration);
		});

    /*==========  Tooltip  ==========*/
    // $('[data-toggle="tooltip"]').tooltip();

    /*==========  Quick Overview Modal  ==========*/
    // $(".quick-view-modal").css("display", "block");




})(jQuery);