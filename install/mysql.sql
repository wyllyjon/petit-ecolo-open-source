SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `alerte` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_envoi` date NOT NULL,
  `frequence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recherche` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_creation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `annonce` (
  `id` int(11) NOT NULL,
  `categorie_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `complex_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `titre` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `monnaie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `monnaie_paiement` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `texte` longtext COLLATE utf8_unicode_ci NOT NULL,
  `troc` tinyint(1) DEFAULT NULL,
  `show_tel` tinyint(1) NOT NULL,
  `date_creation` datetime NOT NULL,
  `date_modif` datetime NOT NULL,
  `confirme` tinyint(1) NOT NULL,
  `validee` tinyint(1) NOT NULL,
  `refusee` tinyint(1) NOT NULL,
  `paiement_effectue` tinyint(1) NOT NULL,
  `pack_urgent` tinyint(1) DEFAULT NULL,
  `pack_photo` tinyint(1) DEFAULT NULL,
  `pack_remontee` tinyint(1) DEFAULT NULL,
  `frequence_remontee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jour_remontee` int(11) DEFAULT NULL,
  `nb_remontees_restantes` int(11) DEFAULT NULL,
  `date_alaune` datetime DEFAULT NULL,
  `date_derniere_mise_alaune` datetime DEFAULT NULL,
  `date_fin_alaune` datetime DEFAULT NULL,
  `taille` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marque` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_mise_circulation` date DEFAULT NULL,
  `energie_vehicule` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boite_vitesse` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kilometrage` int(11) DEFAULT NULL,
  `type_bien` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surface` int(11) DEFAULT NULL,
  `nb_pieces` int(11) DEFAULT NULL,
  `classe_energie` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ges` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meuble` tinyint(1) DEFAULT NULL,
  `capacite_personnes` int(11) DEFAULT NULL,
  `anonymousUser_id` int(11) DEFAULT NULL,
  `taille_enfant` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modele` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_contrat` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remuneration` int(11) DEFAULT NULL,
  `experience` int(11) DEFAULT NULL,
  `temps_plein` tinyint(1) DEFAULT NULL,
  `handi_acces` tinyint(1) DEFAULT NULL,
  `duree` int(11) DEFAULT NULL,
  `date_renvoi_mail_confirmation_creation` datetime DEFAULT NULL,
  `photos` longtext COLLATE utf8_unicode_ci,
  `date_recherche` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `anonymous_user` (
  `id` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `newsletter` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `categorie` (
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position_affichage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `categorie` (`code`, `parent_code`, `nom`, `position_affichage`) VALUES
('animaux', NULL, 'Animaux', 10),
('animaux_adoption', 'animaux', 'Adoption', 0),
('animaux_divers', 'animaux', 'Divers', 9),
('animaux_services', 'animaux', 'Services', 1),
('art', NULL, 'Art et artisanat', 3),
('art_bijoux', 'art', 'Bijoux et accessoires', 4),
('art_divers', 'art', 'Divers', 9),
('art_litterature', 'art', 'Litérature', 2),
('art_nourriture', 'art', 'Nourriture et boissons', 5),
('art_peinture', 'art', 'Peinture', 0),
('art_sculpture', 'art', 'Sculpture', 1),
('art_vetements', 'art', 'Vêtements', 3),
('divers', NULL, 'Divers', 99),
('electronique', NULL, 'Electronique', 9),
('electronique_divers', 'electronique', 'Divers', 9),
('electronique_ordinateurs', 'electronique', 'Ordinateurs', 1),
('electronique_telephone', 'electronique', 'Téléphones', 0),
('emploi', NULL, 'Emplois / stages / Formations', 8),
('emploi_divers', 'emploi', 'Divers', 9),
('emploi_emploi', 'emploi', 'Offre d\'emploi', 0),
('emploi_formation', 'emploi', 'Formation', 2),
('emploi_formationPro', 'emploi', 'Formation Professionnalisante', 3),
('emploi_stage', 'emploi', 'Offre de stage', 1),
('enfants', NULL, 'Enfants', 0),
('enfants_alimentation', 'enfants', 'Alimentation', 5),
('enfants_allaitement', 'enfants', 'Allaitement', 6),
('enfants_divers', 'enfants', 'Divers', 9),
('enfants_hygiene', 'enfants', 'Hygiène et soins', 7),
('enfants_jeux', 'enfants', 'Jeux et loisirs', 1),
('enfants_livres', 'enfants', 'Livres', 3),
('enfants_mobilier', 'enfants', 'Chambre et mobilier', 4),
('enfants_sante', 'enfants', 'Santé et bien-être', 7),
('enfants_securite', 'enfants', 'Sécurité', 8),
('enfants_transport', 'enfants', 'Transport et portage', 2),
('enfants_vetements', 'enfants', 'Vêtements et accessoires', 0),
('immobilier', NULL, 'Immobilier', 7),
('immobilier_colocation', 'immobilier', 'Colocation', 2),
('immobilier_divers', 'immobilier', 'Divers', 9),
('immobilier_location', 'immobilier', 'Location', 1),
('immobilier_vacances', 'immobilier', 'Location de vacances', 3),
('immobilier_vente', 'immobilier', 'Vente', 0),
('jardin', NULL, 'Jardin', 2),
('jardin_divers', 'jardin', 'Divers', 9),
('jardin_outils', 'jardin', 'Outils', 1),
('jardin_semences', 'jardin', 'Semences', 0),
('jardin_terrain', 'jardin', 'Partage de terrain', 2),
('loisirs', NULL, 'Loisirs', 4),
('loisirs_divers', 'loisirs', 'Divers', 9),
('loisirs_evenements', 'loisirs', 'Evénements', 3),
('loisirs_jeux', 'loisirs', 'Jeux', 2),
('loisirs_musiques', 'loisirs', 'Musique', 1),
('loisirs_sport', 'loisirs', 'Sports', 0),
('maison', NULL, 'Maison', 5),
('maison_divers', 'maison', 'Divers', 4),
('maison_electromenager', 'maison', 'Eléctroménager', 1),
('maison_linge', 'maison', 'Linge de maison', 2),
('maison_meubles', 'maison', 'Meubles', 0),
('maison_vetements', 'maison', 'Vêtements et accessoires', 3),
('services', NULL, 'Services', 1),
('services_aidePersonne', 'services', 'Aide à la personne', 1),
('services_chantiers', 'services', 'Chantiers participatifs', 4),
('services_coughsurfing', 'services', 'Couchsurfing', 5),
('services_covoiturage', 'services', 'Covoiturage', 0),
('services_divers', 'services', 'Divers', 9),
('services_domicile', 'services', 'Service à domicile', 2),
('services_materiel', 'services', 'Location de matériel', 6),
('services_reparation', 'services', 'Réparation et rénovation', 3),
('vehicules', NULL, 'Véhicules', 6),
('vehicules_autresEnergies', 'vehicules', 'Autres énergies', 2),
('vehicules_divers', 'vehicules', 'Divers', 9),
('vehicules_electriques', 'vehicules', 'Electriques', 0),
('vehicules_hybrides', 'vehicules', 'Hybrides (essence ou diesel)', 1),
('vehicules_locations', 'vehicules', 'Locations', 3);

CREATE TABLE `photo` (
  `photos` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `stats` (
  `id` int(11) NOT NULL,
  `nb_annonces` int(11) NOT NULL,
  `nb_utilisateurs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `civility` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_pro` longtext COLLATE utf8_unicode_ci,
  `siren` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `annuaire_pro` tinyint(1) DEFAULT NULL,
  `nom_pro` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `users_annoncesFavorites` (
  `user_id` int(11) NOT NULL,
  `annonce_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `users_annoncesSignalees` (
  `user_id` int(11) NOT NULL,
  `annonce_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `alerte`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3AE753AA76ED395` (`user_id`);

ALTER TABLE `annonce`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_F65593E59598FDDD` (`anonymousUser_id`),
  ADD KEY `IDX_F65593E53CB7CF` (`categorie_code`),
  ADD KEY `IDX_F65593E5A76ED395` (`user_id`);
ALTER TABLE `annonce` ADD FULLTEXT KEY `IDX_F65593E5FF7747B4EAE1A6EE` (`titre`,`texte`);

ALTER TABLE `anonymous_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_E7D1242BF5B7AF75` (`address_id`);

ALTER TABLE `categorie`
  ADD PRIMARY KEY (`code`),
  ADD KEY `IDX_497DD6344AF6062C` (`parent_code`);

ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `stats`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`),
  ADD UNIQUE KEY `UNIQ_8D93D649F5B7AF75` (`address_id`);

ALTER TABLE `users_annoncesFavorites`
  ADD PRIMARY KEY (`user_id`,`annonce_id`),
  ADD KEY `IDX_E072DCF2A76ED395` (`user_id`),
  ADD KEY `IDX_E072DCF28805AB2F` (`annonce_id`);

ALTER TABLE `users_annoncesSignalees`
  ADD PRIMARY KEY (`user_id`,`annonce_id`),
  ADD KEY `IDX_D24F5081A76ED395` (`user_id`),
  ADD KEY `IDX_D24F50818805AB2F` (`annonce_id`);


ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `alerte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `annonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `anonymous_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `alerte`
  ADD CONSTRAINT `FK_3AE753AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

ALTER TABLE `annonce`
  ADD CONSTRAINT `FK_F65593E53CB7CF` FOREIGN KEY (`categorie_code`) REFERENCES `categorie` (`code`),
  ADD CONSTRAINT `FK_F65593E59598FDDD` FOREIGN KEY (`anonymousUser_id`) REFERENCES `anonymous_user` (`id`),
  ADD CONSTRAINT `FK_F65593E5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

ALTER TABLE `anonymous_user`
  ADD CONSTRAINT `FK_E7D1242BF5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);

ALTER TABLE `categorie`
  ADD CONSTRAINT `FK_497DD6344AF6062C` FOREIGN KEY (`parent_code`) REFERENCES `categorie` (`code`);

ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649F5B7AF75` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);

ALTER TABLE `users_annoncesFavorites`
  ADD CONSTRAINT `FK_E072DCF28805AB2F` FOREIGN KEY (`annonce_id`) REFERENCES `annonce` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_E072DCF2A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

ALTER TABLE `users_annoncesSignalees`
  ADD CONSTRAINT `FK_D24F50818805AB2F` FOREIGN KEY (`annonce_id`) REFERENCES `annonce` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D24F5081A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

INSERT INTO `stats` (`id`, `nb_annonces`, `nb_utilisateurs`) VALUES (1, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;