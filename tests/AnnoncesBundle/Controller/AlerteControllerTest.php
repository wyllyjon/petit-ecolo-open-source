<?php

namespace Tests\AnnoncesBundle\Controller;


use AnnoncesBundle\Entity\Alerte;
use AnnoncesBundle\Entity\Annonce;
use AnnoncesBundle\Entity\Categorie;
use AppBundle\Entity\Address;
use AppBundle\Entity\AnonymousUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;

class AlerteControllerTest extends WebTestCase
{
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	/**
	 * @var Client
	 */
	private $client;

	private static $uniqId;

	public static function setUpBeforeClass()
	{
		self::$uniqId = uniqid();
	}


	protected function setUp()
	{
		parent::setUp();

		$this->client = static::createClient();

		$this->entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();
	}


	public function testAjouteAlerteAction()
	{
		$this->creeAnnoncesTest();

		// On fait la recherche sur la catégorie "enfants"
		$this->client->followRedirects();
		$crawler = $this->client->request('GET', "/annonces", array(), array(), array(
			'PHP_AUTH_USER' => 'admin',
			'PHP_AUTH_PW'   => 'admin',
		));

		$form = $crawler->filter('form')->form();
		$form["categorie"] = "enfants";
		$crawler = $this->client->submit($form);

		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());


		// On vérifie qu'on a bien plusieurs annonces dans les résultats
		$this->assertGreaterThan(0, $crawler->filter('.ad-listing.annonce')->count());

		// On récupère le formulaire pour créer l'alerte
		$form = $crawler->filter('form#save-alerte-form')->form();
		$form["titre"] = "Alerte de test".self::$uniqId;
		$form["frequence"] = "quotidien";

		$crawler = $this->client->submit($form);
	}

	public function testmesAlertesAction()
	{
		$crawler = $this->client->request('GET', "/alertes/mes-alertes", array(), array(), array(
			'PHP_AUTH_USER' => 'admin',
			'PHP_AUTH_PW'   => 'admin',
		));

		// On vérifie qu'il y a bien une alerte enregistrée
		$this->assertEquals(1, $crawler->filter('.user-archives.alertes .ads-list-archive')->count());

	}

	public function testEnvoiAlertes()
	{
		$this->client->enableProfiler();
		$crawler = $this->client->request('GET', "/alertes/envoi/quotidien");

//		echo $this->client->getResponse()->getContent();

		$this->assertEquals(1, $crawler->filter('body:contains("1 alertes détectées")')->count());

		$mailCollector = $this->client->getProfile()->getCollector('swiftmailer');

		$this->assertSame(1, $mailCollector->getMessageCount());

		$collectedMessages = $mailCollector->getMessages();
		$message = $collectedMessages[0];

		$contenuMessage = $message->getBody();

		// On teste qu'il y a une annonce
		$this->assertNotFalse(strrpos($contenuMessage, "Il y a une nouvelle annonce"));

		// On teste qu'on a bien le titre de l'annonce qui doit ressortir dans l'alerte
		$this->assertNotFalse(strrpos($contenuMessage, "Annonce de test numéro 2 des alertes".self::$uniqId));
	}

	public function testSupprimeAlerte()
	{
		$repoAlerte = $this->entityManager->getRepository('AnnoncesBundle:Alerte');
		$alerte = $repoAlerte->findOneBy(array('titre' => "Alerte de test".self::$uniqId));

		$this->assertEquals(get_class($alerte), Alerte::class);

		$crawler = $this->client->request('POST', "/alertes/supprimer/".$alerte->getId(), array(), array(), array(
			'PHP_AUTH_USER' => 'admin',
			'PHP_AUTH_PW'   => 'admin',
		));

		$alerte = $repoAlerte->findOneBy(array('titre' => "Alerte de test".self::$uniqId));

		$this->assertNull($alerte);

		$this->supprimeAnnoncesTest();
	}

	private function creeAnnoncesTest()
	{
		// Annonce 1 créée le 1 janvier 2016.
		// Ne sortira pas dans les alertes
		$repoCategorie = $this->entityManager->getRepository('AnnoncesBundle:Categorie');
		$categorie = $repoCategorie->findByCode("enfants");

		$annonce1 = new Annonce();
		$annonce1->setComplexId(uniqid());
		$annonce1->setCategorie($categorie);
		$annonce1->setType("particulier");
		$annonce1->setTitre("Annonce de test numéro 1 des alertes".self::$uniqId);
		$annonce1->setDateRecherche(new \DateTime("01/01/2016 14:48:21"));
		$annonce1->setDateCreation(new \DateTime("01/01/2016 14:48:21"));
		$annonce1->setDateModification(new \DateTime("01/01/2016 14:48:21"));
		$annonce1->setTexte("Pas de description");
		$annonce1->setConfirme(true);
		$annonce1->setValidee(true);
		$annonce1->setMonnaie("euro");
		$annonce1->setMonnaiePaiement("euro");
		$annonce1->setShowTel(false);

		$user = new AnonymousUser();
		$address = new Address();

		$address->setAddress("Adresse");
		$address->setPostal("31830");
		$address->setVille("Plaisance");
		$address->setLat("12.22");
		$address->setLng("5646");


		$user->setAdresse($address);
		$user->setEmail("toto@testAlerte.com");
		$user->setNom("Utilisateur test Alerte");

		$annonce1->setAnonymousUser($user);

		// Annonce 2 créée aujourd'hui, ressortira dans les alertes.
		$annonce2 = new Annonce();
		$annonce2->setComplexId(uniqid());
		$annonce2->setMonnaie("euro");
		$annonce2->setMonnaiePaiement("euro");
		$annonce2->setType("particulier");
		$annonce2->setCategorie($categorie);
		$annonce2->setTitre("Annonce de test numéro 2 des alertes".self::$uniqId);
		$annonce2->setDateRecherche(new \DateTime("now"));
		$annonce2->setDateCreation(new \DateTime("now"));
		$annonce2->setDateModification(new \DateTime("now"));
		$annonce2->setTexte("Contenu de l'annonce numéro 2");
		$annonce2->setConfirme(true);
		$annonce2->setValidee(true);
		$annonce2->setShowTel(false);

		$user = new AnonymousUser();
		$address = new Address();

		$address->setAddress("Adresse");
		$address->setPostal("31830");
		$address->setVille("Plaisance");
		$address->setLat("12.22");
		$address->setLng("5646");

		$user->setAdresse($address);
		$user->setEmail("toto@testAlerte.com");
		$user->setNom("Utilisateur test Alerte");

		$annonce2->setAnonymousUser($user);

		$this->entityManager->persist($annonce1);
		$this->entityManager->persist($annonce2);
		$this->entityManager->flush();

	}

	private function supprimeAnnoncesTest()
	{
		$repoAnnonce = $this->entityManager->getRepository('AnnoncesBundle:Annonce');
		$annonce1 = $repoAnnonce->findOneBy(array("titre" => "Annonce de test numéro 1 des alertes".self::$uniqId));
		$annonce2 = $repoAnnonce->findOneBy(array("titre" => "Annonce de test numéro 2 des alertes".self::$uniqId));

		$this->assertNotNull($annonce1);
		$this->assertNotNull($annonce2);

		$this->entityManager->remove($annonce1);
		$this->entityManager->remove($annonce2);

		$this->entityManager->flush();
	}
}