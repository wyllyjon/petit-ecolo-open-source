<?php

namespace Tests\AnnoncesBundle\Controller;


use AnnoncesBundle\Entity\Annonce;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AnnonceControllerTest
 * @package Tests\AnnoncesBundle\Controller
 * @group Annonce
 */
class AnnonceControllerTest extends WebTestCase
{

	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $entityManager;
	private $client;


	protected function setUp()
	{
		parent::setUp();

		$this->client = static::createClient();

		$this->entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();
	}


	/**
	 * Teste la création et la suppression d'une annonce.
	 */
	public function testNewAction()
	{
		// ON CREE UNE ANNONCE
		$crawler = $this->client->request('GET', '/annonces/nouvelle');
		$this->client->followRedirects();
		$identifiant = uniqid();

		$form = $crawler->filter('form')->form();

		$form["categorie"] = "animaux";
		$form["type"] = "demande";
		$form["titre"] = "Annonce de test fonctionnel".$identifiant;

		$form["texte"] = "Description de l'annonce";
		$form["prix"] = "250";
		$form["monnaie"] = "g1";
		$form["troc"] = "1";

		$form["anonymousUser[nom]"] = "Wendlinger Jonathan";
		$form["anonymousUser[email]"] = "test@fonctionnel.com";
		$form["anonymousUser[tel]"] = "0561054687";

		$form["anonymousUser[adresse][address]"] = "29 place du test";
		$form["anonymousUser[adresse][postal]"] = "64230";
		$form["anonymousUser[adresse][ville]"] = "Lescar";
		$form["anonymousUser[adresse][lat]"] = "36.255";
		$form["anonymousUser[adresse][lng]"] = "-0.2154";

		$form["packUrgent"] = 1;
		$form["packPhoto"] = 1;

		$form["valid"] = 1;


		// On submit le formulaire
		$crawler = $this->client->submit($form);

		// On vérifie qu'on a bien un statut 200
		$this->assertEquals(200, $this->client->getResponse()->getStatusCode());

		// On valide l'annonce
		$link = $crawler
			->filter('a:contains("Valider l\'annonce")')
			->link();
		$crawler = $this->client->click($link);

		// On vérifie qu'on a bien un message de succès :
		$this->assertGreaterThan(0, $crawler->filter('.static-panel:contains("a bien été créée")')->count());


		// On vérifie en base qu'on a bien créé l'annonce
		$repo = $this->entityManager->getRepository("AnnoncesBundle:Annonce");
		$annonce = $repo->findOneBy(["titre" => "Annonce de test fonctionnel".$identifiant]);

		$this->assertNotNull($annonce);
		if ($annonce != null)
		{
			// On récupère l'annonce avec les users, adresses, etc.
			$annonce = $repo->find($annonce->getId());

			$this->assertNotNull($annonce);
			$this->assertEquals("demande", $annonce->getType());
			$this->assertEquals("animaux", $annonce->getCategorie()->getCode());
			$this->assertEquals("Description de l'annonce", $annonce->getTexte());
			$this->assertEquals(250, $annonce->getPrix());
			$this->assertEquals("g1", $annonce->getMonnaie());
			$this->assertEquals(1, $annonce->isTroc());
			$this->assertEquals(1, $annonce->hasPackUrgent());

			$user = $annonce->getAnonymousUser();
			$this->assertNotNull($user);
			$this->assertEquals("Wendlinger Jonathan", $user->getNom());
			$this->assertEquals("test@fonctionnel.com", $user->getEmail());
			$this->assertEquals("0561054687", $user->getTel());

			$adresse = $user->getAdresse();
			$this->assertNotNull($user);
			$this->assertEquals("29 place du test", $adresse->getAddress());
			$this->assertEquals("64230", $adresse->getPostal());
			$this->assertEquals("Lescar", $adresse->getVille());
			$this->assertEquals("36.255", $adresse->getLat());
			$this->assertEquals("-0.2154", $adresse->getLng());
		}

		// ON SUPPRIME L'ANNONCE
		$crawler = $this->client->request('GET', '/annonces/voir/'.$annonce->getId().'/titre/'.$annonce->getComplexId());
		$form = $crawler->selectButton('Supprimer')->form();
		$crawler = $this->client->submit($form);

		// On vérifie qu'elle n'existe plus en base
		$annonce = $repo->findOneBy(["titre" => "Annonce de test fonctionnel".$identifiant]);
		$this->assertNull($annonce);
	}
}