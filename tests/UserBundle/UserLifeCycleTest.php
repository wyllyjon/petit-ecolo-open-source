<?php

namespace Tests\UserBundle;


use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class UserLifeCycleTest
 * @package Tests\UserBundle
 * @group User
 */
class UserLifeCycleTest extends WebTestCase
{

	private $client;
	private static $uniqId;
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $entityManager;

	public static function setUpBeforeClass()
	{
		self::$uniqId = uniqid();
	}

	protected function setUp()
	{
		parent::setUp();
		$this->client = static::createClient();
		$this->client->followRedirects();

		$this->entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();
	}

	public function testUserCreation()
	{
		$crawler = $this->client->request('GET', "/register/");
		$form = $crawler->filter('form')->form();


		$form["fos_user_registration_form[type]"] = "particulier";
		$form["fos_user_registration_form[email]"] = "test@fonctionnel.fr".self::$uniqId;
		$form["fos_user_registration_form[username]"] = "testFonctionnel".self::$uniqId;
		$form["fos_user_registration_form[plainPassword][first]"] = "password";
		$form["fos_user_registration_form[plainPassword][second]"] = "password";

		// On submit le formulaire
		$crawler = $this->client->submit($form);

		// On vérifie qu'on a bien un message de succès :
		$this->assertGreaterThan(0, $crawler->filter('.registration_message:contains("Un e-mail a été envoyé à l\'adresse")')->count());
	}

	public function testActivateAccount()
	{
		$repo = $this->entityManager->getRepository(User::class);
		$user = $repo->findOneBy(array("username" => "testFonctionnel".self::$uniqId));

		$token = $user->getConfirmationToken();

		$crawler = $this->client->request('GET', "/register/confirm/".$token);

		$this->assertGreaterThan(0, $crawler->filter('.registration_message:contains("votre compte est maintenant activé")')->count());

	}

	public function testModifAccount()
	{

		$crawler = $this->client->request('GET', "/profile/edit", array(), array(), array(
			'PHP_AUTH_USER' => 'testFonctionnel'.self::$uniqId,
			'PHP_AUTH_PW'   => 'password',
		));

		$form = $crawler->filter('form')->form();

		$this->assertEquals("test@fonctionnel.fr".self::$uniqId, $form["fos_user_profile_form[email]"]->getValue());

		$newMail = "testmodifie@fonctionnel.fr".self::$uniqId;
		$form["fos_user_profile_form[email]"] = $newMail;
		$form["fos_user_profile_form[adresse][postal]"] = "31830";
		$form["fos_user_profile_form[adresse][ville]"] = "Plaisance du Touch";
		$form["fos_user_profile_form[adresse][lat]"] = "32.2254";
		$form["fos_user_profile_form[adresse][lng]"] = "0.1245";
		$form["fos_user_profile_form[current_password]"] = "password";

		// On submit le formulaire
		$crawler = $this->client->submit($form);

		$this->assertGreaterThan(0, $crawler->filter('.dl-horizontal:contains("'.$newMail.'")')->count());

	}

	public function testDeleteAccount()
	{
		$crawler = $this->client->request('GET', "/profile/", array(), array(), array(
			'PHP_AUTH_USER' => 'testFonctionnel'.self::$uniqId,
			'PHP_AUTH_PW'   => 'password',
		));

		$link = $crawler->filter('a:contains("Supprimer mon compte")')->link();
		$crawler = $this->client->click($link);

		// On teste que l'utilisateur n'existe plus
		$repo = $this->entityManager->getRepository(User::class);
		$user = $repo->findOneBy(array("username" => "testFonctionnel".self::$uniqId));

		$this->assertNull($user);
	}

}