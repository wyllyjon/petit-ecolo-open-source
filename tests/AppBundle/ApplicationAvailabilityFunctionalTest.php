<?php

namespace Tests\AppBundle;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationAvailabilityFunctionalTest extends WebTestCase
{

	private $client = null;

	public function setUp()
	{
		$this->client = static::createClient();
	}


	/**
	 * @dataProvider urlProvider
	 */
	public function testPageIsSuccessful($url)
	{
		$client = self::createClient();
		$client->request('GET', $url);
		$response = $client->getResponse();
		$this->assertTrue($response->isSuccessful());
	}

	public function urlProvider()
	{
		return array(
			array(''),
			array('/annonces/'),
			array('/annonces/nouvelle'),
			array('/contact'),
			array('/login'),
			array('/resetting/request'),
			array('/page/respect-donnees-personnelles'),
			array('/page/mentions'),
			array('/page/cgu'),
			array('/page/cgv'),
			array('/page/cookies'),
			// ...
		);
	}



	/**
	 * @dataProvider urlRedirectionProvider
	 */
	public function testPageRedirectionIsSuccessful($url)
	{
		$this->client->request('GET', $url);

		$this->assertTrue($this->client->getResponse()->isRedirection());
	}

	public function urlRedirectionProvider()
	{
		return array(
			array('/logout'),
			array('/register'),
			// ...
		);
	}


	/**
	 * @dataProvider urlAuthenticatedProvider
	 */
	public function testAuthenticatedPageIsSuccessful($url)
	{
//		$this->logIn();

		$this->client->request('GET', $url, array(), array(), array(
			'PHP_AUTH_USER' => 'admin',
			'PHP_AUTH_PW'   => 'admin',
		));

		$this->assertTrue($this->client->getResponse()->isSuccessful());
	}

	public function urlAuthenticatedProvider()
	{
		return array(
			array('/profile/'),
			array('/annonces/mes-annonces'),
			array('/annonces/favorites'),
			array('/profile/edit'),
			// ...
		);
	}
}
