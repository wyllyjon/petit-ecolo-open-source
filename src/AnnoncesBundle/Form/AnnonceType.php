<?php

namespace AnnoncesBundle\Form;

use AjaxUploadBundle\Form\Type\AjaxfileType;
use AnnoncesBundle\Entity\Annonce;
use AppBundle\Form\AnonymousUserType;
use AppBundle\Form\UserType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class AnnonceType extends AbstractType
{

	private $autorizationChecker;

	public function __construct(AuthorizationChecker  $autorizationChecker)
	{
		$this->autorizationChecker = $autorizationChecker;
	}

    /**
     * {@inheritdoc}
     */
		public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('type', ChoiceType::class, array(
				'choices' => array(
					'annonces.form.type_offre' => 'offre',
					'annonces.form.type_demande' => 'demande',
				),
				'data' => 'offre',
				'expanded' => true,
				'label' => false,
			))
			->add('categorie', EntityType::class, array(
				'block_name' => 'categories',
				'class' => 'AnnoncesBundle\Entity\Categorie',
				'required' => true,
				'attr' => array('class' => 'category form-control'),
				'choice_label' => function ($categorie) {
					if ($categorie->getParent() == '')
					{
						return "----- ".$categorie->getNom()." -----";
					}
					else
					{
						return $categorie->getNom();
					}
				},
				'choice_value' => 'code',
				'choice_attr' => function($val, $key, $index) {
					if ($val->getParent() === null)
					{
						return ['data-class' => 'parent'];
					}
					return ['class' => ''];
				}
			))
//			->add('categorie', EntityType::class, array(
//				'label' => 'annonces.form.categorie_label',
//				'class' => 'AnnoncesBundle\Entity\Categorie',
//				'choice_label' => 'nom',
//				'choice_value' => 'id',
//				'choice_attr' => function($val, $key, $index) {
//					// adds a class like attending_yes, attending_no, etc
//					if ($val->getParent() === null)
//					{
//						return ['data-class' => 'parent'];
//					}
//					return ['class' => ''];
//				}
//			))
			->add('titre')
			->add('texte', TextareaType::class, array('label' => 'annonces.form.text_label'))

//			->add('photos', CollectionType::class, array(
//				'entry_type' => PhotoType::class,
//				'label' => false,
//				'allow_add'    => true,
//				'allow_delete' => true,
//				'prototype'    => true,
//				'required'     => false,
//				'entry_options'  => array('label' => false),
//				'attr' => array('parentClass' => 'divPhoto')
//			))
			->add('photos', HiddenType::class, array('error_bubbling' => false, 'required' => false))
			->add('bnbc_ajax_file_photos', FileType::class, array('attr' => array('parentClass' => 'photoFileInput'), 'multiple' => true, 'mapped'=> false, 'required' => false))



			->add('prix', MoneyType::class, array(
//						'constraints' => array(
//							new NotBlank(array('groups' => array('prix'), 'message' => 'annonces.form.errors.prix')),
//						),
					'currency' => 'g1',
					'attr' => array('parentClass' => 'champPrix'),
					'required' => false,
				)
			)
			->add('monnaie', ChoiceType::class, array(
				'choices' => array(
					'annonces.form.monnaie_g1' => Annonce::$MONNAIE_G1,
					'annonces.form.monnaie_dug1' => Annonce::$MONNAIE_DU_G1,
					'annonces.form.monnaie_euro' => Annonce::$MONNAIE_EURO,
				),
//				'data' => 'euro',
				'expanded' => false,
				'label' => 'annonces.form.monnaie_label',
			))
			->add('troc', CheckboxType::class, array('label' => 'annonces.form.troc_label', 'required' => false))
			->add('taille', TextType::class, array('label' => "annonces.view.taille", 'attr' => array('parentClass' => 'champTaille'), 'required' => false))
			->add('tailleEnfant', TextType::class, array('label' => "annonces.view.taille", 'attr' => array('parentClass' => 'champTaille'), 'required' => false));

		// VEHICULES
		$builder->add('marque', ChoiceType::class, array(
			'choices' => array(
				'Audi' => Annonce::$MARQUE_VEHICULE_AUDI,
				'BMW' => Annonce::$MARQUE_VEHICULE_BMW,
				'Cadillac' => Annonce::$MARQUE_VEHICULE_CADILLAC,
				'Chevrolet' => Annonce::$MARQUE_VEHICULE_CHEVROLET,
				'Citroen' => Annonce::$MARQUE_VEHICULE_CITROEN,
				'Ford' => Annonce::$MARQUE_VEHICULE_FORD,
				'Honda' => Annonce::$MARQUE_VEHICULE_HONDA,
				'Hyundai' => Annonce::$MARQUE_VEHICULE_HYUNDAI,
				'Kia' => Annonce::$MARQUE_VEHICULE_KIA,
				'Lexus' => Annonce::$MARQUE_VEHICULE_LEXUS,
				'Mercedes' => Annonce::$MARQUE_VEHICULE_MERCEDES,
				'Mitsubishi' => Annonce::$MARQUE_VEHICULE_MITSUBISHI ,
				'Nissan' => Annonce::$MARQUE_VEHICULE_NISSAN,
				'Peugeot' => Annonce::$MARQUE_VEHICULE_PEUGEOT ,
				'Porsche' => Annonce::$MARQUE_VEHICULE_PORSCHE,
				'Range Rover' => Annonce::$MARQUE_VEHICULE_RANGE_ROVER,
				'Renault' => Annonce::$MARQUE_VEHICULE_RENAULT,
				'Smart' => Annonce::$MARQUE_VEHICULE_SMART,
				'Suzuki' => Annonce::$MARQUE_VEHICULE_SUZUKI,
				'Tesla' => Annonce::$MARQUE_VEHICULE_TESLA,
				'Toyota' => Annonce::$MARQUE_VEHICULE_TOYOTA,
				'Volkswagen' => Annonce::$MARQUE_VEHICULE_VOLKSWAGEN,
				'Volvo' => Annonce::$MARQUE_VEHICULE_VOLVO,
				'annonces.form.voiture.marques.autre' => Annonce::$MARQUE_VEHICULE_AUTRE
			),
			'required' => false,
			'label' => 'annonces.form.voiture.marque_label',
		));
		$builder->add('modele', TextType::class, array('required' => false, 'label' => 'annonces.form.voiture.modele_label'));

		$builder->add('dateMiseCirculation', DateType::class, array('widget' => 'single_text', 'label' => 'annonces.form.voiture.date_mise_circulation', 'required' => false));
//			->add('energieVehicule', ChoiceType::class, array(
//				'choices' => array(
//					'annonces.form.voiture.energie.electrique' => 'electrique',
//					'annonces.form.voiture.energie.hybride' => 'hybride',
//					'annonces.form.voiture.energie.autre' => 'autre',
//				),
//				'label' => 'annonces.form.voiture.energie.titre',
//				'required' => false
//			))
		$builder->add('boiteVitesse', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.voiture.boite_vitesse.manuelle' => Annonce::$BOITE_VITESSE_MANUELLE,
				'annonces.form.voiture.boite_vitesse.auto' => Annonce::$BOITE_VITESSE_AUTO,
			),
			'required' => false,
			'label' => 'annonces.form.voiture.boite_vitesse.titre',
		));
		$builder->add('kilometrage', IntegerType::class, array('label' => 'annonces.form.voiture.kilometrage', 'required' => false));



		// IMMOBILIER
		$builder->add('typeBien', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.immobilier.type_bien.maison' => Annonce::$TYPE_BIEN_MAISON,
				'annonces.form.immobilier.type_bien.appartement' => Annonce::$TYPE_BIEN_APPART,
				'annonces.form.immobilier.type_bien.terrain' => Annonce::$TYPE_BIEN_TERRAIN,
				'annonces.form.immobilier.type_bien.autre' => Annonce::$TYPE_BIEN_AUTRE
			),
			'required' => false,
			'label' => 'annonces.form.immobilier.type_bien.titre',
		));
		$builder->add('surface', IntegerType::class, array('label' => 'annonces.form.immobilier.surface', 'required' => false))
			->add('nbPieces', IntegerType::class, array('label' => 'annonces.form.immobilier.nb_pieces', 'required' => false));
		$builder->add('classeEnergie', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.immobilier.classe_energie.A' => Annonce::$CLASSE_ENERGIE_A,
				'annonces.form.immobilier.classe_energie.B' => Annonce::$CLASSE_ENERGIE_B,
				'annonces.form.immobilier.classe_energie.C' => Annonce::$CLASSE_ENERGIE_C,
				'annonces.form.immobilier.classe_energie.D' => Annonce::$CLASSE_ENERGIE_D,
				'annonces.form.immobilier.classe_energie.E' => Annonce::$CLASSE_ENERGIE_E,
				'annonces.form.immobilier.classe_energie.F' => Annonce::$CLASSE_ENERGIE_F,
				'annonces.form.immobilier.classe_energie.G' => Annonce::$CLASSE_ENERGIE_G,
				'annonces.form.immobilier.classe_energie.H' => Annonce::$CLASSE_ENERGIE_H,
				'annonces.form.immobilier.classe_energie.I' => Annonce::$CLASSE_ENERGIE_I,
			),
			'required' => false,
			'label' => 'annonces.form.immobilier.classe_energie.titre',
		))
		;$builder->add('ges', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.immobilier.ges.A' => Annonce::$CLASSE_ENERGIE_A,
				'annonces.form.immobilier.ges.B' => Annonce::$CLASSE_ENERGIE_B,
				'annonces.form.immobilier.ges.C' => Annonce::$CLASSE_ENERGIE_C,
				'annonces.form.immobilier.ges.D' => Annonce::$CLASSE_ENERGIE_D,
				'annonces.form.immobilier.ges.E' => Annonce::$CLASSE_ENERGIE_E,
				'annonces.form.immobilier.ges.F' => Annonce::$CLASSE_ENERGIE_F,
				'annonces.form.immobilier.ges.G' => Annonce::$CLASSE_ENERGIE_G,
				'annonces.form.immobilier.ges.H' => Annonce::$CLASSE_ENERGIE_H,
				'annonces.form.immobilier.ges.I' => Annonce::$CLASSE_ENERGIE_I,
			),
			'required' => false,
			'label' => 'annonces.form.immobilier.ges.titre',
		));
		$builder->add('meuble', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.immobilier.meuble.meuble' => Annonce::$BIEN_MEUBLE,
				'annonces.form.immobilier.meuble.vide' => Annonce::$BIEN_NON_MEUBLE,
			),
			'required' => false,
			'label' => 'annonces.form.immobilier.meuble.titre',
		));
		$builder->add('capacitePersonnes', IntegerType::class, array('label' => 'annonces.form.immobilier.capacite', 'required' => false));

		// EMPLOI
		$builder->add('typeContrat', ChoiceType::class, array(
			'choices' => array(
				'main_search.options_categories.type_contrat.cdi' => Annonce::$TYPE_CONTRAT_CDI,
				'main_search.options_categories.type_contrat.cdd' => Annonce::$TYPE_CONTRAT_CDD,
				'main_search.options_categories.type_contrat.freelance' => Annonce::$TYPE_CONTRAT_FREELANCE,
				'main_search.options_categories.type_contrat.interim' => Annonce::$TYPE_CONTRAT_INTERIM,
				'main_search.options_categories.type_contrat.autre' => Annonce::$TYPE_CONTRAT_AUTRE,
			),
			'required' => false,
			'label' => 'main_search.options_categories.type_contrat.placeholder',
		));
		$builder->add('remuneration', IntegerType::class, array('required' => false, 'label' => 'annonces.form.emploi.remuneration'));
		$builder->add('experience', IntegerType::class, array('required' => false, 'label' => 'annonces.form.emploi.experience'));
		$builder->add('tempsPlein', CheckboxType::class, array('required' => false, 'label' => 'main_search.options_categories.temps_plein'));
		$builder->add('handiAcces', CheckboxType::class, array('required' => false, 'label' => 'main_search.options_categories.handi_acces'));
		$builder->add('duree', IntegerType::class, array('required' => false, 'label' => 'annonces.form.emploi.duree_stage'));



		// On gère l'ajout d'un user / anonymousUser
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
			$annonce = $event->getData();
			$form = $event->getForm();

			// C'est une nouvelle annonce, on affiche le champs en fonction de si l'utilisateur est connecté ou non.
			if (!$annonce || null === $annonce->getId()) {
				if (!$this->autorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED'))
				{
					$form->add('anonymousUser', AnonymousUserType::class, ['label' => false]);
				}
				else
				{
					$form->add('user', UserType::class, ['label' => false]);
				}
			}
			// On affiche le champ qui a été rempli lors de la création de l'annonce
			else
			{
				if ($annonce->getUser())
				{
					$form->add('user', UserType::class, ['label' => false]);
				}
				else
				{
					$form->add('anonymousUser', AnonymousUserType::class, ['label' => false]);
				}
			}
		});


		$builder->add('showTel', CheckboxType::class, array('label' => 'annonces.form.affiche_tel_label', 'required' => false));

        // OPTIONS

		$builder->add('packRemontee', CheckboxType::class, array('label' => 'annonces.form.options.pack_remontee', 'required' => false))
				->add('packUrgent', CheckboxType::class, array('label' => 'annonces.form.options.pack_urgent', 'required' => false))
				->add('frequenceRemontee', ChoiceType::class, array(
							'choices' => array(
								'annonces.form.options.frequence_jour' => Annonce::$FREQUENCE_REMONTEE_JOUR,
								'annonces.form.options.frequence_semaine' => Annonce::$FREQUENCE_REMONTEE_SEMAINE,
							),
							'label' => 'annonces.form.options.frequence_remontee',
							'required' => false
						))
				->add('nbRemonteesRestantes', IntegerType::class, array('label' => 'annonces.form.options.nb_remontee', 'required' => false));

        $builder->add("valid", CheckboxType::class, array('mapped' => false));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AnnoncesBundle\Entity\Annonce',
			'spam_protection' => true,
			'spam_protection_time' => 15,
//			'validation_groups' => function (FormInterface $form) {
//				$data = $form->getData();
//
//				if ($data->getCategorie() == '13') {
//					return array('Default', 'emploi');
//				}
//
//				return array('Default', 'prix');
//			},
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }


}
