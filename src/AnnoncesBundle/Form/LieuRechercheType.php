<?php

namespace AnnoncesBundle\Form;

use AppBundle\Form\AnonymousUserType;
use AppBundle\Form\UserType;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class LieuRechercheType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lieu', TextType::class, array('label' => false,'required' => false, 'attr' => array('placeholder' => 'main_search.lieu_placeholder', 'class' => "form-control")))
			->add('lieuExact', ChoiceType::class, array(
				'choices' => array(
					'main_search.lieu_exact_checkbox' => 'exact',
					'main_search.lieu_distance_a' => 'rayon',
				),
//				'choice_attr' => function($val, $key, $index) {
//					if ($val == 'exact')
//					return ['onchange' => '$(\'#lieu_distance\').attr(\'disabled\', \'disabled\');'];
//					else
//					return ['onchange' => "$('#lieu_distance').removeAttr('disabled');"];
//				},
				'data' => 'exact',
				'expanded' => true,
				'label' => false,
			))
			->add('type', HiddenType::class)
			->add('departement', HiddenType::class)
			->add('distance', TextType::class, array('label' => false, 'required' => false, 'data' => '20', 'attr' => array('class' => 'lieu_distance form-control')))
        	->add('bbox', HiddenType::class)
			->add('coord', HiddenType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AnnoncesBundle\Entity\LieuRecherche',
        ));
    }

}
