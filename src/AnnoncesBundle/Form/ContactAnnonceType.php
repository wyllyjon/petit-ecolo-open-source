<?php

namespace AnnoncesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ContactAnnonceType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
			->add('nom', TextType::class)
			->add('mail', EmailType::class)
			->add('message', TextareaType::class)
			->add('spsd', TextType::class, array('required' => false, 'attr' => array("parentClass"=> "spsd_field")))
			->add('save', SubmitType::class, array('label' => 'annonces.contact.submit_label'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AnnoncesBundle\Entity\ContactAnnonce',
			'spam_protection' => true,
			'spam_protection_time' => 7
        ));
    }

}
