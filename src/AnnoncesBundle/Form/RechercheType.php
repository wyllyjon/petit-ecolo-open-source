<?php

namespace AnnoncesBundle\Form;

use AnnoncesBundle\Entity\Annonce;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RechercheType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('categorie', EntityType::class, array(
			'label' => false,
			'block_name' => 'categories',
			'class' => 'AnnoncesBundle\Entity\Categorie',
			'required' => false,
			'attr' => array('class' => 'category form-control'),
			'choice_label' => function ($categorie) {
				if ($categorie->getParent() == '')
				{
					return "----- ".$categorie->getNom()." -----";
				}
				else
				{
					return $categorie->getNom();
				}
			},
			'choice_value' => 'code',
			'choice_attr' => function($val, $key, $index) {
				if ($val->getParent() === null)
				{
					return ['data-class' => 'parent'];
				}
				return ['class' => ''];
			}
		))
			->add('recherche', TextType::class, array(
				'required' => false,
				'label' => false,
				'attr' => array('class' => 'form-control', 'placeholder' => 'main_search.keyword_placeholder'),
			))
			->add('lieu', LieuRechercheType::class)
			->add('monnaie', ChoiceType::class, array(
				'choices' => array(
					'annonces.form.monnaie_g1' => Annonce::$MONNAIE_G1,
					'annonces.form.monnaie_euro' => Annonce::$MONNAIE_EURO,
				),
				'expanded' => false,
				'required' => false,
				'label' => false,
			))
			->add('troc', CheckboxType::class, array('required' => false, 'label' => 'main_search.troc_label'))
			->add('urgent', CheckboxType::class, array('required' => false, 'label' => 'main_search.urgent_label'))
			->add('page', HiddenType::class)
			->add('nb_resultats', HiddenType::class)
			->add('save', SubmitType::class, array('label' => 'main_search.bouton_label', 'attr' => array('class' => 'btn btn-danger btn-lg btn-block', 'onclick' => '$(\'#page_input\').val(\'1	\')')))
			->setMethod('get');

        // Recherche avancée

		$builder->add('prixMax', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.prix_max_label')));
		$builder->add('prixMin', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.prix_min_label')));

		// Options des categories

		$builder->add('taille', TextType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'annonces.form.taille_placeholder')));
		$builder->add('tailleEnfant', TextType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'annonces.form.taille_placeholder')));

		// Emploi / stages
		$builder->add('typeContrat', ChoiceType::class, array(
			'choices' => array(
				'main_search.options_categories.type_contrat.cdi' => Annonce::$TYPE_CONTRAT_CDI,
				'main_search.options_categories.type_contrat.cdd' => Annonce::$TYPE_CONTRAT_CDD,
				'main_search.options_categories.type_contrat.freelance' => Annonce::$TYPE_CONTRAT_FREELANCE,
				'main_search.options_categories.type_contrat.interim' => Annonce::$TYPE_CONTRAT_INTERIM,
				'main_search.options_categories.type_contrat.autre' => Annonce::$TYPE_CONTRAT_AUTRE,
			),
			'required' => false,
			'label' => false,
			'attr' => array('placeholder' => 'main_search.options_categories.type_contrat.placeholder')
		));
		$builder->add('remunerationMin', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.remuneration')));
		$builder->add('experienceMax', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.experience')));
		$builder->add('tempsPlein', CheckboxType::class, array('required' => false, 'label' => 'main_search.options_categories.temps_plein'));
		$builder->add('tempsPartiel', CheckboxType::class, array('required' => false, 'label' => 'main_search.options_categories.temps_partiel'));
		$builder->add('handiAcces', CheckboxType::class, array('required' => false, 'label' => 'main_search.options_categories.handi_acces'));
		$builder->add('dureeMin', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.duree_stage')));
		$builder->add('dureeMax', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.duree_stage')));

		// Immobilier
		$builder->add('typeBien', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.immobilier.type_bien.maison' => Annonce::$TYPE_BIEN_MAISON,
				'annonces.form.immobilier.type_bien.appartement' => Annonce::$TYPE_BIEN_APPART,
				'annonces.form.immobilier.type_bien.terrain' => Annonce::$TYPE_BIEN_TERRAIN,
				'annonces.form.immobilier.type_bien.autre' => Annonce::$TYPE_BIEN_AUTRE
			),
			'required' => false,
			'label' => false,
			'expanded' => true,
			'multiple' => true,
			'attr' => array('placeholder' => 'annonces.form.immobilier.type_bien.titre')
		));
		$builder->add('surfaceMin', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.surface_min')));
		$builder->add('surfaceMax', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.surface_max')));
		$builder->add('nbPiecesMax', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.pieces_max')));
		$builder->add('nbPiecesMin', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.pieces_min')));
//		$builder->add('classEnergie', ChoiceType::class, array(
//			'choices' => array(
//				'annonces.form.immobilier.classe_energie.A' => Annonce::$CLASSE_ENERGIE_A,
//				'annonces.form.immobilier.classe_energie.B' => Annonce::$CLASSE_ENERGIE_B,
//				'annonces.form.immobilier.classe_energie.C' => Annonce::$CLASSE_ENERGIE_C,
//				'annonces.form.immobilier.classe_energie.D' => Annonce::$CLASSE_ENERGIE_D,
//				'annonces.form.immobilier.classe_energie.E' => Annonce::$CLASSE_ENERGIE_E,
//				'annonces.form.immobilier.classe_energie.F' => Annonce::$CLASSE_ENERGIE_F,
//				'annonces.form.immobilier.classe_energie.G' => Annonce::$CLASSE_ENERGIE_G,
//				'annonces.form.immobilier.classe_energie.H' => Annonce::$CLASSE_ENERGIE_H,
//				'annonces.form.immobilier.classe_energie.I' => Annonce::$CLASSE_ENERGIE_I,
//			),
//			'required' => false,
//			'label' => false,
//			'attr' => array('placeholder' => 'annonces.form.immobilier.classe_energie.titre')
//		))
//		;$builder->add('ges', ChoiceType::class, array(
//			'choices' => array(
//				'annonces.form.immobilier.ges.A' => Annonce::$CLASSE_ENERGIE_A,
//				'annonces.form.immobilier.ges.B' => Annonce::$CLASSE_ENERGIE_B,
//				'annonces.form.immobilier.ges.C' => Annonce::$CLASSE_ENERGIE_C,
//				'annonces.form.immobilier.ges.D' => Annonce::$CLASSE_ENERGIE_D,
//				'annonces.form.immobilier.ges.E' => Annonce::$CLASSE_ENERGIE_E,
//				'annonces.form.immobilier.ges.F' => Annonce::$CLASSE_ENERGIE_F,
//				'annonces.form.immobilier.ges.G' => Annonce::$CLASSE_ENERGIE_G,
//				'annonces.form.immobilier.ges.H' => Annonce::$CLASSE_ENERGIE_H,
//				'annonces.form.immobilier.ges.I' => Annonce::$CLASSE_ENERGIE_I,
//			),
//			'required' => false,
//			'label' => false,
//			'attr' => array('placeholder' => 'annonces.form.immobilier.ges.titre')
//		));
		$builder->add('meuble', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.immobilier.meuble.meuble' => Annonce::$BIEN_MEUBLE,
				'annonces.form.immobilier.meuble.vide' => Annonce::$BIEN_NON_MEUBLE,
			),
			'required' => false,
			'label' => false,
			'attr' => array('placeholder' => 'annonces.form.immobilier.meuble.titre')
		));
		$builder->add("capacite", IntegerType::class, array("required" => false, "label" => false, 'attr' => array('placeholder' => 'main_search.options_categories.capacite')));



		// VEHICULES
		$builder->add('marque', ChoiceType::class, array(
			'choices' => array(
				'Audi' => Annonce::$MARQUE_VEHICULE_AUDI,
				'BMW' => Annonce::$MARQUE_VEHICULE_BMW,
				'Cadillac' => Annonce::$MARQUE_VEHICULE_CADILLAC,
				'Chevrolet' => Annonce::$MARQUE_VEHICULE_CHEVROLET,
				'Citroen' => Annonce::$MARQUE_VEHICULE_CITROEN,
				'Ford' => Annonce::$MARQUE_VEHICULE_FORD,
				'Honda' => Annonce::$MARQUE_VEHICULE_HONDA,
				'Hyundai' => Annonce::$MARQUE_VEHICULE_HYUNDAI,
				'Kia' => Annonce::$MARQUE_VEHICULE_KIA,
				'Lexus' => Annonce::$MARQUE_VEHICULE_LEXUS,
				'Mercedes' => Annonce::$MARQUE_VEHICULE_MERCEDES,
				'Mitsubishi' => Annonce::$MARQUE_VEHICULE_MITSUBISHI ,
				'Nissan' => Annonce::$MARQUE_VEHICULE_NISSAN,
				'Peugeot' => Annonce::$MARQUE_VEHICULE_PEUGEOT ,
				'Porsche' => Annonce::$MARQUE_VEHICULE_PORSCHE,
				'Range Rover' => Annonce::$MARQUE_VEHICULE_RANGE_ROVER,
				'Renault' => Annonce::$MARQUE_VEHICULE_RENAULT,
				'Smart' => Annonce::$MARQUE_VEHICULE_SMART,
				'Suzuki' => Annonce::$MARQUE_VEHICULE_SUZUKI,
				'Tesla' => Annonce::$MARQUE_VEHICULE_TESLA,
				'Toyota' => Annonce::$MARQUE_VEHICULE_TOYOTA,
				'Volkswagen' => Annonce::$MARQUE_VEHICULE_VOLKSWAGEN,
				'Volvo' => Annonce::$MARQUE_VEHICULE_VOLVO,
				'annonces.form.voiture.marques.autre' => Annonce::$MARQUE_VEHICULE_AUTRE
			),
			'required' => false,
			'label' => false,
			'attr' => array('placeholder' => 'annonces.form.voiture.marque_label')
		));
		$builder->add('modele', TextType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'annonces.form.voiture.modele_label')));
		$builder->add('anneeMin', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.annee_min')));
		$builder->add('kilometrageMax', IntegerType::class, array('required' => false, 'label' => false, 'attr' => array('placeholder' => 'main_search.options_categories.kilometrage_max')));
		$builder->add('boiteVitesse', ChoiceType::class, array(
			'choices' => array(
				'annonces.form.voiture.boite_vitesse.manuelle' => Annonce::$BOITE_VITESSE_MANUELLE,
				'annonces.form.voiture.boite_vitesse.auto' => Annonce::$BOITE_VITESSE_AUTO,
			),
			'required' => false,
			'label' => false,
			'attr' => array('placeholder' => 'annonces.form.voiture.boite_vitesse.titre')
		));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AnnoncesBundle\Entity\Recherche',
			'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return null;
    }

}
