<?php

namespace AnnoncesBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class LieuRecherche
{
	private $lieu;

	private $bbox;

	private $coord;

	// country,region,postcode,place,locality
	/**
	 */
	private $type;

	// string
	private $departement;

	// Integer
	private $distance;

	// Boolean
	private $lieuExact;

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param mixed $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return mixed
	 */
	public function getDepartement()
	{
		return $this->departement;
	}

	/**
	 * @param mixed $departement
	 */
	public function setDepartement($departement)
	{
		$this->departement = $departement;
	}




	/**
	 * @return mixed
	 */
	public function getLieu()
	{
		return $this->lieu;
	}

	/**
	 * @param mixed $lieu
	 */
	public function setLieu($lieu)
	{
		$this->lieu = $lieu;
	}

	/**
	 * @return mixed
	 */
	public function getBbox()
	{
		return explode(',', $this->bbox);
	}

	public function getRawBbox()
	{
		if (is_array($this->bbox))
		{
			return implode(',',$this->bbox);
		}
		return $this->bbox;
	}

	/**
	 * @param mixed $bbox
	 */
	public function setBbox($bbox)
	{
		$this->bbox = $bbox;
	}

	/**
	 * @return mixed
	 */
	public function getCoord()
	{
		return explode(',', $this->coord);
	}

	public function getRawCoord()
	{
		if (is_array($this->coord))
		{
			return implode(',',$this->coord);
		}
		return $this->coord;
	}

	/**
	 * @param mixed $coord
	 */
	public function setCoord($coord)
	{
		$this->coord = $coord;
	}


	/**
	 * @return mixed
	 */
	public function getDistance()
	{
		return $this->distance;
	}

	/**
	 * @param mixed $distance
	 */
	public function setDistance($distance)
	{
		$this->distance = $distance;
	}

	/**
	 * @return mixed
	 */
	public function getLieuExact()
	{
		return $this->lieuExact;
	}

	/**
	 * @param mixed $lieuExact
	 */
	public function setLieuExact($lieuExact)
	{
		$this->lieuExact = $lieuExact;
	}

	public function isLieuExact()
	{
		return $this->lieuExact === 'exact';
	}

	/**
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{
		if ($this->lieu != '' and $this->bbox == '') {
			$context->buildViolation('annonces.form.errors.lieu')
				->atPath('lieu')
				->addViolation();
		}
	}

}

