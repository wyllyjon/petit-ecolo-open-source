<?php

namespace AnnoncesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="AnnoncesBundle\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="position_affichage", type="integer")
	 */
    private $positionAffichage;

	/**
	 * @var string
     * @ORM\Id
	 * @ORM\Column(name="code", type="string", length=255)
	 */
	private $code;


	/**
	 * Many Categories have One Category.
	 * @ORM\ManyToOne(targetEntity="AnnoncesBundle\Entity\Categorie", inversedBy="enfants")
	 * @ORM\JoinColumn(name="parent_code", referencedColumnName="code", nullable=true)
	 */
	private $parent;

	/**
	 * One Category has Many children Categories.
	 * @ORM\OneToMany(targetEntity="AnnoncesBundle\Entity\Categorie", mappedBy="parent", cascade={"remove"})
	 */
	private $enfants;

	public function __construct() {
		$this->enfants = new \Doctrine\Common\Collections\ArrayCollection();

	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * @param string $code
	 */
	public function setCode($code)
	{
		$this->code = $code;
	}

	/**
	 * @return int
	 */
	public function getPositionAffichage()
	{
		return $this->positionAffichage;
	}

	/**
	 * @param int $positionAffichage
	 */
	public function setPositionAffichage($positionAffichage)
	{
		$this->positionAffichage = $positionAffichage;
	}

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

	/**
	 * Get nom complet
	 *
	 * @return string
	 */
	public function getNomComplet()
	{
		if ($this->parent)
		{
			return $this->parent->getNom() . " > " . $this-> nom;
		}
		return $this->nom;
	}

    /**
     * Set parent
     *
     * @param Categorie $parent
     *
     * @return Categorie
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Categorie
     */
    public function getParent()
    {
        return $this->parent;
    }

	/**
	 * @return mixed
	 */
	public function getEnfants()
	{
		return $this->enfants;
	}

	/**
	 * @param mixed $enfants
	 */
	public function setEnfants($enfants)
	{
		$this->enfants = $enfants;
	}

	public function __toString()
	{
		return "".$this->code;
	}

}

