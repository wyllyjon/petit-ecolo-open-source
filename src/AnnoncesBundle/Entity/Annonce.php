<?php

namespace AnnoncesBundle\Entity;

use AppBundle\Entity\AnonymousUser;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Annonce
 *
 * @ORM\Table(name="annonce",indexes={@ORM\Index(columns={"titre", "texte"}, flags={"fulltext"})})
 * @ORM\Entity(repositoryClass="AnnoncesBundle\Repository\AnnonceRepository")
 */
class Annonce
{
	public static $MONNAIE_EURO = 'euro';
	public static $MONNAIE_G1 = 'g1';
	public static $MONNAIE_DU_G1 = 'dug1';

	public static $TYPE_CONTRAT_CDD = 'cdd';
	public static $TYPE_CONTRAT_CDI = 'cdi';
	public static $TYPE_CONTRAT_INTERIM = 'interim';
	public static $TYPE_CONTRAT_FREELANCE = 'freelance';
	public static $TYPE_CONTRAT_AUTRE = 'autre';

	public static $TYPE_BIEN_MAISON = 'maison';
	public static $TYPE_BIEN_APPART = 'appart';
	public static $TYPE_BIEN_TERRAIN = 'terrain';
	public static $TYPE_BIEN_AUTRE = 'autre';

	public static $BIEN_MEUBLE = 'meuble';
	public static $BIEN_NON_MEUBLE = 'vide';

	public static $CLASSE_ENERGIE_A = 'a';
	public static $CLASSE_ENERGIE_B = 'b';
	public static $CLASSE_ENERGIE_C = 'c';
	public static $CLASSE_ENERGIE_D = 'd';
	public static $CLASSE_ENERGIE_E = 'e';
	public static $CLASSE_ENERGIE_F = 'f';
	public static $CLASSE_ENERGIE_G = 'g';
	public static $CLASSE_ENERGIE_H = 'h';
	public static $CLASSE_ENERGIE_I = 'i';


	public static $MARQUE_VEHICULE_AUDI = "audi";
	public static $MARQUE_VEHICULE_BMW = "BMW";
	public static $MARQUE_VEHICULE_CADILLAC = "cadillac";
	public static $MARQUE_VEHICULE_CHEVROLET = "chevrolet";
	public static $MARQUE_VEHICULE_CITROEN = "citroen";
	public static $MARQUE_VEHICULE_FORD = "ford";
	public static $MARQUE_VEHICULE_HONDA = "honda";
	public static $MARQUE_VEHICULE_HYUNDAI = "hyundai";
	public static $MARQUE_VEHICULE_KIA = "kia";
	public static $MARQUE_VEHICULE_LEXUS = "lexus";
	public static $MARQUE_VEHICULE_MERCEDES = "mercedes";
	public static $MARQUE_VEHICULE_MITSUBISHI = "mitsubishi";
	public static $MARQUE_VEHICULE_NISSAN = "nissan";
	public static $MARQUE_VEHICULE_PEUGEOT = "peugeot";
	public static $MARQUE_VEHICULE_PORSCHE = "porsche";
	public static $MARQUE_VEHICULE_RANGE_ROVER = "rangerover";
	public static $MARQUE_VEHICULE_RENAULT = "renault";
	public static $MARQUE_VEHICULE_SMART = "smart";
	public static $MARQUE_VEHICULE_SUZUKI = "suzuki";
	public static $MARQUE_VEHICULE_TESLA = "tesla";
	public static $MARQUE_VEHICULE_TOYOTA = "toyota";
	public static $MARQUE_VEHICULE_VOLKSWAGEN = "volkswagen";
	public static $MARQUE_VEHICULE_VOLVO = "volvo";
	public static $MARQUE_VEHICULE_AUTRE = "autre";

	public static $BOITE_VITESSE_MANUELLE = "manuelle";
	public static $BOITE_VITESSE_AUTO = "auto";

	public static $FREQUENCE_REMONTEE_JOUR = "jour";
	public static $FREQUENCE_REMONTEE_SEMAINE = "semaine";


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="complex_id", type="string", length=30)
	 */
    private $complexId;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=500)
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity="AnnoncesBundle\Entity\Categorie")
	 * @ORM\JoinColumn(name="categorie_code", referencedColumnName="code")
	 * @Assert\NotBlank(message="Merci de renseigner la catégorie")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prix;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="monnaie", type="string", length=30)
	 * g1 ou euro
	 */
	private $monnaie;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="texte", type="text")

     */
    private $texte;

	/**
	 * @var bool
	 * Indique si l'annonceur est ouvert au troc
	 *
	 * @ORM\Column(name="troc", type="boolean", nullable=true)
	 */
	private $troc;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="photos", type="text", nullable=true)

	 */
	private $photos;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="annonces")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
	 *
	 * @Assert\Valid
	 *
	 **/
    private $user;

	/**
	 * @ORM\OneToOne(targetEntity="AppBundle\Entity\AnonymousUser", cascade={"persist"})
	 * @ORM\JoinColumn(name="anonymousUser_id", referencedColumnName="id", nullable=true)
	 *
	 * @Assert\Valid
	 *
	 */
    private $anonymousUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_tel", type="boolean")
     */
    private $showTel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;


	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_modif", type="datetime")
	 */
	private $dateModification;


	/**
	 * Date utilisée pour le tri des résultats de recherche
	 * C'est cette date qui sera modifiée lors des remontées en tête de liste automatiques
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_recherche", type="datetime", nullable=true)
	 */
	private $dateRecherche;


	/**
	 * Date à laquelle l'admin a renvoyer un mail à l'utilisateur à propos de l'annonce qu'il n'a pas validée en étape 2
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_renvoi_mail_confirmation_creation", type="datetime", nullable=true)
	 */
	private $dateRenvoiMailConfirmationCreation;

	/**
	 * Utilisateurs ayant cette annonce en favoris
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="annoncesFavorites")
	 */
	private $usersFavoris;


	/**
	 * Utilisateurs ayant signalé cette annonce
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="annoncesSignalees")
	 */
	private $usersSignales;


	/**
	 * @var bool
	 * Indique si l'annonce a été confirmée par l'utilisateur (étape 2 de création d'une annonce)
	 *
	 * @ORM\Column(name="confirme", type="boolean")
	 */
	private $confirme = false;


	/**
	 * @var bool
	 * Indique si l'annonce a été validée par le modérateur
	 *
	 * @ORM\Column(name="validee", type="boolean")
	 */
	private $validee = false;


	/**
	 * @var bool
	 * Indique si l'annonce a été refusée par le modérateur
	 *
	 * @ORM\Column(name="refusee", type="boolean")
	 */
	private $refusee = false;


	/**
	 * @var bool
	 * Indique si l'annonce possède un pack urgent (logo urgent sur l'annonce)
	 *
	 * @ORM\Column(name="pack_urgent", type="boolean", nullable=true)
	 */
	private $packUrgent;


	/**
	 * @var bool
	 * Indique si l'annonce est premium (remontée auto + pack photo)
	 *
	 * @ORM\Column(name="pack_remontee", type="boolean", nullable=true)
	 */
	private $packRemontee;

	/**
	 * @var string
	 * Indique la fréquence de remontée automatique en tête de liste (jour / semaine)
	 *
	 * @ORM\Column(name="frequence_remontee", type="string", nullable=true)
	 */
	private $frequenceRemontee;

	/**
	 * @var int
	 *
	 * Indique le jour pour les remontées hebdomadaires. (si un utilisateur poste une annonce le samedi, il ne faut pas qu'elle remonte le dimanche, mais le samedi d'après)
	 * Utilise le numéro de jour de la semaine de la fonction date en php.
	 *
	 * @ORM\Column(name="jour_remontee", type="integer", nullable=true)
	 */
	private $jourRemontee;

	/**
	 * @var int
	 * Indique la nombre de remontées restantes
	 *
	 * @ORM\Column(name="nb_remontees_restantes", type="integer", nullable=true)
	 *
	 * @Assert\Range(
	 *      max = 100,
	 *      maxMessage = "Le nombre maximum de remontées est de {{ limit }}",
	 *      invalidMessage = "Merci de rentrer un chiffre entre 1 et 100"
	 * )
	 *
	 */
	private $nbRemonteesRestantes;

	/**
	 * @var \DateTime
	 * Indique si l'annonce a une date de début de mise à la une
	 *
	 * @ORM\Column(name="date_alaune", type="datetime", nullable=true)
	 */
	private $dateAlaune;


	/**
	 * @var \DateTime
	 * Indique quand l'annonce a été mise à la une pour la dernière fois
	 *
	 * @ORM\Column(name="date_derniere_mise_alaune", type="datetime", nullable=true)
	 */
	private $dateDerniereMiseAlaune;

	/**
	 * @var \DateTime
	 * Indique la date de fin de mise à la une
	 *
	 * @ORM\Column(name="date_fin_alaune", type="datetime", nullable=true)
	 */
	private $dateFinAlaune;



	// Champs conditionnels des catégories

	/* VETEMENTS */

	/**
	 * @var string
	 *
	 * @ORM\Column(name="taille", type="string", length=50, nullable=true)
	 */
	private $taille;
	/**
	 * @var string
	 *
	 * @ORM\Column(name="taille_enfant", type="string", length=50, nullable=true)
	 */
	private $tailleEnfant;


	/* AUTOMOBILES */

	/**
	 * @var string
	 *
	 * @ORM\Column(name="marque", type="string", length=150, nullable=true)
	 */
	private $marque;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="modele", type="string", length=150, nullable=true)
	 */
	private $modele;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="date_mise_circulation", type="date", length=150, nullable=true)
	 */
	private $dateMiseCirculation;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="energie_vehicule", type="string", length=20, nullable=true)
	 */
	private $energieVehicule;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="boite_vitesse", type="string", length=10, nullable=true)
	 */
	private $boiteVitesse;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="kilometrage", type="integer", length=10, nullable=true)
	 */
	private $kilometrage;

	/* IMMOBILIER */

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type_bien", type="string", length=15, nullable=true)
	 */
	private $typeBien;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="surface", type="integer", length=5, nullable=true)
	 */
	private $surface;


	/**
	 * @var integer
	 *
	 * @ORM\Column(name="nb_pieces", type="integer", length=3, nullable=true)
	 */
	private $nbPieces;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="classe_energie", type="string", length=10, nullable=true)
	 */
	private $classeEnergie;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ges", type="string", length=10, nullable=true)
	 */
	private $ges;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="meuble", type="boolean", length=10, nullable=true)
	 */
	private $meuble;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="capacite_personnes", type="integer", length=5, nullable=true)
	 */
	private $capacitePersonnes;


	// EMPLOI
	/**
	 * @var string
	 *
	 * @ORM\Column(name="type_contrat", type="string", length=10, nullable=true)
	 */
	private $typeContrat;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="remuneration", type="integer", length=100, nullable=true)
	 */
	private $remuneration; // NETTE Annuelle !
	/**
	 * @var string
	 *
	 * @ORM\Column(name="experience", type="integer", length=100, nullable=true)
	 */
	private $experience;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="temps_plein", type="boolean", length=10, nullable=true)
	 */

	private $tempsPlein; //Boolean
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="handi_acces", type="boolean", length=10, nullable=true)
	 */
	private $handiAcces; // Accessible aux handicapés : Boolean

	/**
	 * @var string
	 *
	 * @ORM\Column(name="duree", type="integer", length=100, nullable=true)
	 */
	private $duree; // Pour les stages

	public function __construct() {
		$this->usersFavoris = new ArrayCollection();
	}

	/**
	 * @return bool
	 */
	public function isTroc()
	{
		return $this->troc;
	}

	/**
	 * @param bool $troc
	 */
	public function setTroc($troc)
	{
		$this->troc = $troc;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateRenvoiMailConfirmationCreation()
	{
		return $this->dateRenvoiMailConfirmationCreation;
	}

	/**
	 * @param \DateTime $dateRenvoiMailConfirmationCreation
	 */
	public function setDateRenvoiMailConfirmationCreation($dateRenvoiMailConfirmationCreation)
	{
		$this->dateRenvoiMailConfirmationCreation = $dateRenvoiMailConfirmationCreation;
	}


	/**
	 * @return string
	 */
	public function getMonnaie()
	{
		return $this->monnaie;
	}

	/**
	 * @param string $monnaie
	 */
	public function setMonnaie($monnaie)
	{
		$this->monnaie = $monnaie;
	}


	/**
	 * @return bool
	 */
	public function hasPackRemontee()
	{
		return $this->packRemontee;
	}

	/**
	 * @param bool $packRemontee
	 */
	public function setPackRemontee($packRemontee)
	{
		$this->packRemontee = $packRemontee;
		if ($packRemontee)
		{
			$this->packPhoto = true;
		}
	}

	/**
	 * @return string
	 */
	public function getFrequenceRemontee()
	{
		return $this->frequenceRemontee;
	}

	/**
	 * @param string $frequenceRemontee
	 */
	public function setFrequenceRemontee($frequenceRemontee)
	{
		$this->frequenceRemontee = $frequenceRemontee;
	}

	/**
	 * @return int
	 */
	public function getNbRemonteesRestantes()
	{
		return $this->nbRemonteesRestantes;
	}

	/**
	 * @param int $nbRemonteesRestantes
	 */
	public function setNbRemonteesRestantes($nbRemonteesRestantes)
	{
		$this->nbRemonteesRestantes = $nbRemonteesRestantes;
	}

	/**
	 * @return int
	 */
	public function getJourRemontee()
	{
		return $this->jourRemontee;
	}

	/**
	 * @param int $jourRemontee
	 */
	public function setJourRemontee($jourRemontee)
	{
		$this->jourRemontee = $jourRemontee;
	}


	/**
	 * @return mixed
	 */
	public function getUsersFavoris()
	{
		return $this->usersFavoris;
	}

	/**
	 * @param mixed $usersFavoris
	 */
	public function setUsersFavoris($usersFavoris)
	{
		$this->usersFavoris = $usersFavoris;
	}

	/**
	 * @param User $user
	 * @return bool
	 */
	public function isFavoris(User $user)
	{
		return $this->usersFavoris->contains($user);
	}

	/**
	 * @param User $user
	 * @return bool
	 */
	public function isSignalee(User $user)
	{
		return $this->usersSignales->contains($user);
	}



	/**
	 * @return string
	 */
	public function getMeuble()
	{
		return $this->meuble;
	}

	/**
	 * @param string $meuble
	 */
	public function setMeuble($meuble)
	{
		$this->meuble = $meuble;
	}



	/**
	 * @return string
	 */
	public function getTaille()
	{
		return $this->taille;
	}

	/**
	 * @param string $taille
	 */
	public function setTaille($taille)
	{
		$this->taille = $taille;
	}




	/**
	 * @return bool
	 */
	public function hasPackUrgent()
	{
		return $this->packUrgent;
	}

	/**
	 * @param bool $packUrgent
	 */
	public function setPackUrgent($packUrgent)
	{
		$this->packUrgent = $packUrgent;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateAlaune()
	{
		return $this->dateAlaune;
	}

	/**
	 * @param \DateTime $dateAlaune
	 */
	public function setDateAlaune($dateAlaune)
	{
		$this->dateAlaune = $dateAlaune;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateDerniereMiseAlaune()
	{
		return $this->dateDerniereMiseAlaune;
	}

	/**
	 * @param \DateTime $dateDerniereMiseAlaune
	 */
	public function setDateDerniereMiseAlaune($dateDerniereMiseAlaune)
	{
		$this->dateDerniereMiseAlaune = $dateDerniereMiseAlaune;
	}


	/**
	 * @return \DateTime
	 */
	public function getDateFinAlaune()
	{
		return $this->dateFinAlaune;
	}

	/**
	 * @param \DateTime $dateFinAlaune
	 */
	public function setDateFinAlaune($dateFinAlaune)
	{
		$this->dateFinAlaune = $dateFinAlaune;
	}

	public function isALaUne()
	{
		if ($this->dateAlaune <= new \DateTime() and $this->dateFinAlaune >= new \DateTime())
		{
			return true;
		}
		return false;
	}

	/**
	 * @return bool
	 */
	public function isConfirme()
	{
		return $this->confirme;
	}

	/**
	 * @param bool $confirme
	 */
	public function setConfirme($confirme)
	{
		$this->confirme = $confirme;
	}

	/**
	 * @return bool
	 */
	public function isValidee()
	{
		return $this->validee;
	}

	/**
	 * @param bool $validee
	 */
	public function setValidee($validee)
	{
		$this->validee = $validee;
	}

	/**
	 * @return bool
	 */
	public function isRefusee()
	{
		return $this->refusee;
	}

	/**
	 * @param bool $refusee
	 */
	public function setRefusee($refusee)
	{
		$this->refusee = $refusee;
	}


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getComplexId()
	{
		return $this->complexId;
	}

	/**
	 * @param string $complexId
	 */
	public function setComplexId($complexId)
	{
		$this->complexId = $complexId;
	}


	/**
     * Set titre
     *
     * @param string $titre
     *
     * @return Annonce
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set categorie
     *
     * @param \stdClass $categorie
     *
     * @return Annonce
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \stdClass
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return Annonce
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Annonce
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set texte
     *
     * @param string $texte
     *
     * @return Annonce
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get texte
     *
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Set photos
     *
     * @return Annonce
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * Get photos
     *
     */
    public function getPhotos()
    {
    	return $this->photos;
    }


    /**
     * Set user
     *
     * @param \stdClass $user
     *
     * @return Annonce
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set anonymousUser
     *
     * @param AnonymousUser $anonymousUser
     *
     * @return Annonce
     */
    public function setAnonymousUser($anonymousUser)
    {
        $this->anonymousUser = $anonymousUser;

        return $this;
    }

    /**
     * Get anonymousUser
     *
     * @return \stdClass
     */
    public function getAnonymousUser()
    {
        return $this->anonymousUser;
    }

    /**
     * Set showTel
     *
     * @param boolean $showTel
     *
     * @return Annonce
     */
    public function setShowTel($showTel)
    {
        $this->showTel = $showTel;

        return $this;
    }

    /**
     * Get showTel
     *
     * @return bool
     */
    public function getShowTel()
    {
        return $this->showTel;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Annonce
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

	/**
	 * @return \DateTime
	 */
	public function getDateModification()
	{
		return $this->dateModification;
	}

	/**
	 * @param \DateTime $dateModification
	 */
	public function setDateModification($dateModification)
	{
		$this->dateModification = $dateModification;
	}

	/**
	 * @return string
	 */
	public function getDateMiseCirculation()
	{
		return $this->dateMiseCirculation;
	}

	/**
	 * @param string $dateMiseCirculation
	 */
	public function setDateMiseCirculation($dateMiseCirculation)
	{
		$this->dateMiseCirculation = $dateMiseCirculation;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateRecherche()
	{
		return $this->dateRecherche;
	}

	/**
	 * @param \DateTime $dateRecherche
	 */
	public function setDateRecherche(\DateTime $dateRecherche)
	{
		$this->dateRecherche = $dateRecherche;
	}

	/**
	 * @return string
	 */
	public function getEnergieVehicule()
	{
		return $this->energieVehicule;
	}

	/**
	 * @param string $energieVehicule
	 */
	public function setEnergieVehicule($energieVehicule)
	{
		$this->energieVehicule = $energieVehicule;
	}

	/**
	 * @return string
	 */
	public function getBoiteVitesse()
	{
		return $this->boiteVitesse;
	}

	/**
	 * @param string $boiteVitesse
	 */
	public function setBoiteVitesse($boiteVitesse)
	{
		$this->boiteVitesse = $boiteVitesse;
	}

	/**
	 * @return int
	 */
	public function getKilometrage()
	{
		return $this->kilometrage;
	}

	/**
	 * @param int $kilometrage
	 */
	public function setKilometrage($kilometrage)
	{
		$this->kilometrage = $kilometrage;
	}

	/**
	 * @return string
	 */
	public function getTypeBien()
	{
		return $this->typeBien;
	}

	/**
	 * @param string $typeBien
	 */
	public function setTypeBien($typeBien)
	{
		$this->typeBien = $typeBien;
	}

	/**
	 * @return int
	 */
	public function getSurface()
	{
		return $this->surface;
	}

	/**
	 * @param int $surface
	 */
	public function setSurface($surface)
	{
		$this->surface = $surface;
	}

	/**
	 * @return int
	 */
	public function getNbPieces()
	{
		return $this->nbPieces;
	}

	/**
	 * @param int $nbPieces
	 */
	public function setNbPieces($nbPieces)
	{
		$this->nbPieces = $nbPieces;
	}

	/**
	 * @return string
	 */
	public function getClasseEnergie()
	{
		return $this->classeEnergie;
	}

	/**
	 * @param string $classeEnergie
	 */
	public function setClasseEnergie($classeEnergie)
	{
		$this->classeEnergie = $classeEnergie;
	}

	/**
	 * @return string
	 */
	public function getGes()
	{
		return $this->ges;
	}

	/**
	 * @param string $ges
	 */
	public function setGes($ges)
	{
		$this->ges = $ges;
	}

	/**
	 * @return int
	 */
	public function getCapacitePersonnes()
	{
		return $this->capacitePersonnes;
	}

	/**
	 * @param int $capacitePersonnes
	 */
	public function setCapacitePersonnes($capacitePersonnes)
	{
		$this->capacitePersonnes = $capacitePersonnes;
	}

	/**
	 * @return string
	 */
	public function getTailleEnfant()
	{
		return $this->tailleEnfant;
	}

	/**
	 * @param string $tailleEnfant
	 */
	public function setTailleEnfant($tailleEnfant)
	{
		$this->tailleEnfant = $tailleEnfant;
	}

	/**
	 * @return string
	 */
	public function getMarque()
	{
		return $this->marque;
	}

	/**
	 * @param string $marque
	 */
	public function setMarque($marque)
	{
		$this->marque = $marque;
	}

	/**
	 * @return string
	 */
	public function getModele()
	{
		return $this->modele;
	}

	/**
	 * @param string $modele
	 */
	public function setModele($modele)
	{
		$this->modele = $modele;
	}

	/**
	 * @return string
	 */
	public function getTypeContrat()
	{
		return $this->typeContrat;
	}

	/**
	 * @param string $typeContrat
	 */
	public function setTypeContrat($typeContrat)
	{
		$this->typeContrat = $typeContrat;
	}

	/**
	 * @return string
	 */
	public function getRemuneration()
	{
		return $this->remuneration;
	}

	/**
	 * @param string $remuneration
	 */
	public function setRemuneration($remuneration)
	{
		$this->remuneration = $remuneration;
	}

	/**
	 * @return string
	 */
	public function getExperience()
	{
		return $this->experience;
	}

	/**
	 * @param string $experience
	 */
	public function setExperience($experience)
	{
		$this->experience = $experience;
	}

	/**
	 * @return bool
	 */
	public function isTempsPlein()
	{
		return $this->tempsPlein;
	}

	/**
	 * @param bool $tempsPlein
	 */
	public function setTempsPlein($tempsPlein)
	{
		$this->tempsPlein = $tempsPlein;
	}

	/**
	 * @return bool
	 */
	public function isHandiAcces()
	{
		return $this->handiAcces;
	}

	/**
	 * @param bool $handiAcces
	 */
	public function setHandiAcces($handiAcces)
	{
		$this->handiAcces = $handiAcces;
	}

	/**
	 * @return string
	 */
	public function getDuree()
	{
		return $this->duree;
	}

	/**
	 * @param string $duree
	 */
	public function setDuree($duree)
	{
		$this->duree = $duree;
	}



	/**
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{
		if ($this->packRemontee && ($this->nbRemonteesRestantes == '' || $this->frequenceRemontee == ''))
		{
			$context->buildViolation('annonces.form.errors.option_remontee')
				->atPath('packRemontee')->addViolation();
		}

		$nbPhotos = count(explode(";", $this->photos));

		if ($nbPhotos > 10)
		{
			$context->buildViolation('annonces.form.errors.photos_max')
				->atPath('photos')->addViolation();
		}
	}
}

