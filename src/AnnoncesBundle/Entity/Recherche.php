<?php

namespace AnnoncesBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class Recherche
{
	private $PAGE_DEFAULT_VALUE = 1;
	private $NB_RESULTATS_DEFAULT_VALUE = 10;

	/**
	 * @var Categorie $categorie
	 *
	 */
	private $categorie;

	// String
	private $recherche;

	/**
	 * @var LieuRecherche $lieu
	 *
	 * @Assert\Valid()
	 *
	 */
	private $lieu;

	private $page;

	private $nb_resultats;

	private $troc;

	/**
	 * Pour n'afficher que les annonces urgentes
	 */
	private $urgent;

	private $monnaie;

	private $prixMin;
	private $prixMax;

	// Champs de recherche supplémentaires selon catégorie

	// Vetement
	private $tailleEnfant;
	private $taille;

	// Emploi
	private $typeContrat;
	private $remunerationMin; // Annuelle !
	private $experienceMax;
	private $tempsPlein; //Boolean
	private $tempsPartiel; //Boolean
	private $handiAcces; // Accessible aux handicapés : Boolean
	private $dureeMin; // Pour les stages
	private $dureeMax; // Pour les stages

	// Immobilier
	private $typeBien; // Maison, appartement, terrain, etc.
	private $surfaceMin;
	private $surfaceMax;
	private $nbPiecesMin;
	private $nbPiecesMax;
//	private $classEnergie;
//	private $ges;
	private $meuble; // Boolean
	private $capacite; // Nb de personne pour location de vacance.

	// Vehicules
	private $marque;
	private $modele;
	private $anneeMin;
	private $kilometrageMax;
	private $boiteVitesse; //Manuelle / auto
	private $energie;

	/**
	 * @return mixed
	 */
	public function getTypeContrat()
	{
		return $this->typeContrat;
	}

	/**
	 * @param mixed $typeContrat
	 */
	public function setTypeContrat($typeContrat)
	{
		$this->typeContrat = $typeContrat;
	}

	/**
	 * @return mixed
	 */
	public function getRemunerationMin()
	{
		return $this->remunerationMin;
	}

	/**
	 * @param mixed $remunerationMin
	 */
	public function setRemunerationMin($remunerationMin)
	{
		$this->remunerationMin = $remunerationMin;
	}

	/**
	 * @return mixed
	 */
	public function getExperienceMax()
	{
		return $this->experienceMax;
	}

	/**
	 * @param mixed $experienceMax
	 */
	public function setExperienceMax($experienceMax)
	{
		$this->experienceMax = $experienceMax;
	}

	/**
	 * @return mixed
	 */
	public function getTempsPlein()
	{
		return $this->tempsPlein;
	}

	/**
	 * @param mixed $tempsPlein
	 */
	public function setTempsPlein($tempsPlein)
	{
		$this->tempsPlein = $tempsPlein;
	}

	/**
	 * @return mixed
	 */
	public function getTempsPartiel()
	{
		return $this->tempsPartiel;
	}

	/**
	 * @param mixed $tempsPartiel
	 */
	public function setTempsPartiel($tempsPartiel)
	{
		$this->tempsPartiel = $tempsPartiel;
	}



	/**
	 * @return mixed
	 */
	public function getHandiAcces()
	{
		return $this->handiAcces;
	}

	/**
	 * @param mixed $handiAcces
	 */
	public function setHandiAcces($handiAcces)
	{
		$this->handiAcces = $handiAcces;
	}

	/**
	 * @return mixed
	 */
	public function getDureeMin()
	{
		return $this->dureeMin;
	}

	/**
	 * @param mixed $dureeMin
	 */
	public function setDureeMin($dureeMin)
	{
		$this->dureeMin = $dureeMin;
	}

	/**
	 * @return mixed
	 */
	public function getDureeMax()
	{
		return $this->dureeMax;
	}

	/**
	 * @param mixed $dureeMax
	 */
	public function setDureeMax($dureeMax)
	{
		$this->dureeMax = $dureeMax;
	}




	/**
	 * @return mixed
	 */
	public function getTypeBien()
	{
		return $this->typeBien;
	}

	/**
	 * @param mixed $typeBien
	 */
	public function setTypeBien($typeBien)
	{
		$this->typeBien = $typeBien;
	}

	/**
	 * @return mixed
	 */
	public function getSurfaceMin()
	{
		return $this->surfaceMin;
	}

	/**
	 * @param mixed $surfaceMin
	 */
	public function setSurfaceMin($surfaceMin)
	{
		$this->surfaceMin = $surfaceMin;
	}

	/**
	 * @return mixed
	 */
	public function getSurfaceMax()
	{
		return $this->surfaceMax;
	}

	/**
	 * @param mixed $surfaceMax
	 */
	public function setSurfaceMax($surfaceMax)
	{
		$this->surfaceMax = $surfaceMax;
	}

	/**
	 * @return mixed
	 */
	public function getNbPiecesMin()
	{
		return $this->nbPiecesMin;
	}

	/**
	 * @param mixed $nbPiecesMin
	 */
	public function setNbPiecesMin($nbPiecesMin)
	{
		$this->nbPiecesMin = $nbPiecesMin;
	}

	/**
	 * @return mixed
	 */
	public function getNbPiecesMax()
	{
		return $this->nbPiecesMax;
	}

	/**
	 * @param mixed $nbPiecesMax
	 */
	public function setNbPiecesMax($nbPiecesMax)
	{
		$this->nbPiecesMax = $nbPiecesMax;
	}




	/**
	 * @return mixed
	 */
//	public function getClassEnergie()
//	{
//		return $this->classEnergie;
//	}
//
//	/**
//	 * @param mixed $classEnergie
//	 */
//	public function setClassEnergie($classEnergie)
//	{
//		$this->classEnergie = $classEnergie;
//	}

	/**
	 * @return mixed
	 */
//	public function getGes()
//	{
//		return $this->ges;
//	}
//
//	/**
//	 * @param mixed $ges
//	 */
//	public function setGes($ges)
//	{
//		$this->ges = $ges;
//	}

	/**
	 * @return mixed
	 */
	public function getMeuble()
	{
		return $this->meuble;
	}

	/**
	 * @param mixed $meuble
	 */
	public function setMeuble($meuble)
	{
		$this->meuble = $meuble;
	}

	/**
	 * @return mixed
	 */
	public function getCapacite()
	{
		return $this->capacite;
	}

	/**
	 * @param mixed $capacite
	 */
	public function setCapacite($capacite)
	{
		$this->capacite = $capacite;
	}

	/**
	 * @return mixed
	 */
	public function getMarque()
	{
		return $this->marque;
	}

	/**
	 * @param mixed $marque
	 */
	public function setMarque($marque)
	{
		$this->marque = $marque;
	}

	/**
	 * @return mixed
	 */
	public function getModele()
	{
		return $this->modele;
	}

	/**
	 * @param mixed $modele
	 */
	public function setModele($modele)
	{
		$this->modele = $modele;
	}

	/**
	 * @return mixed
	 */
	public function getAnneeMin()
	{
		return $this->anneeMin;
	}

	/**
	 * @param mixed $anneeMin
	 */
	public function setAnneeMin($anneeMin)
	{
		$this->anneeMin = $anneeMin;
	}

	/**
	 * @return mixed
	 */
	public function getKilometrageMax()
	{
		return $this->kilometrageMax;
	}

	/**
	 * @param mixed $kilometrageMax
	 */
	public function setKilometrageMax($kilometrageMax)
	{
		$this->kilometrageMax = $kilometrageMax;
	}

	/**
	 * @return mixed
	 */
	public function getBoiteVitesse()
	{
		return $this->boiteVitesse;
	}

	/**
	 * @param mixed $boiteVitesse
	 */
	public function setBoiteVitesse($boiteVitesse)
	{
		$this->boiteVitesse = $boiteVitesse;
	}

	/**
	 * @return mixed
	 */
	public function getEnergie()
	{
		return $this->energie;
	}

	/**
	 * @param mixed $energie
	 */
	public function setEnergie($energie)
	{
		$this->energie = $energie;
	}



	/**
	 * @return mixed
	 */
	public function getTaille()
	{
		return $this->taille;
	}

	/**
	 * @param mixed $taille
	 */
	public function setTaille($taille)
	{
		$this->taille = $taille;
	}

	/**
	 * @return mixed
	 */
	public function getTailleEnfant()
	{
		return $this->tailleEnfant;
	}

	/**
	 * @param mixed $tailleEnfant
	 */
	public function setTailleEnfant($tailleEnfant)
	{
		$this->tailleEnfant = $tailleEnfant;
	}



	/**
	 * @return mixed
	 */
	public function getPrixMin()
	{
		return $this->prixMin;
	}

	/**
	 * @param mixed $prixMin
	 */
	public function setPrixMin($prixMin)
	{
		$this->prixMin = $prixMin;
	}

	/**
	 * @return mixed
	 */
	public function getPrixMax()
	{
		return $this->prixMax;
	}

	/**
	 * @param mixed $prixMax
	 */
	public function setPrixMax($prixMax)
	{
		$this->prixMax = $prixMax;
	}



	/**
	 * @return mixed
	 */
	public function getPage()
	{
		if ($this->page != null)
		{
			return $this->page;
		}
		else
		{
			return $this->PAGE_DEFAULT_VALUE;
		}
	}

	/**
	 * @param mixed $page
	 */
	public function setPage($page)
	{
		$this->page = $page;
	}

	/**
	 * @return mixed
	 */
	public function getNbResultats()
	{
		if ($this->nb_resultats != null)
		{
			return $this->nb_resultats;
		}
		else
		{
			return $this->NB_RESULTATS_DEFAULT_VALUE;
		}
	}

	/**
	 * @param mixed $nb_resultats
	 */
	public function setNbResultats($nb_resultats)
	{
		$this->nb_resultats = $nb_resultats;
	}

	/**
	 * @return mixed
	 */
	public function getTroc()
	{
		return $this->troc;
	}

	/**
	 * @param mixed $troc
	 */
	public function setTroc($troc)
	{
		$this->troc = $troc;
	}

	/**
	 * @return mixed
	 */
	public function getMonnaie()
	{
		return $this->monnaie;
	}

	/**
	 * @param mixed $monnaie
	 */
	public function setMonnaie($monnaie)
	{
		$this->monnaie = $monnaie;
	}

	/**
	 * @return mixed
	 */
	public function getCategorie()
	{
		return $this->categorie;
	}

	/**
	 * @param mixed $categorie
	 */
	public function setCategorie($categorie)
	{
		$this->categorie = $categorie;
	}

	/**
	 * @return mixed
	 */
	public function getRecherche()
	{
		return $this->recherche;
	}

	/**
	 * @param mixed $recherche
	 */
	public function setRecherche($recherche)
	{
		$this->recherche = $recherche;
	}

	/**
	 * @return mixed
	 */
	public function getLieu()
	{
		return $this->lieu;
	}

	/**
	 * @param mixed $lieu
	 */
	public function setLieu($lieu)
	{
		$this->lieu = $lieu;
	}

	/**
	 * @return mixed
	 */
	public function getUrgent()
	{
		return $this->urgent;
	}

	/**
	 * @param mixed $urgent
	 */
	public function setUrgent($urgent): void
	{
		$this->urgent = $urgent;
	}

	

	public function getQueryString()
	{
		$query = array();
		$query["categorie"] = $this->categorie ? $this->categorie->getCode() : "";
		$query["recherche"] = $this->recherche;
		$query["lieu"]["lieu"] = $this->lieu ? $this->lieu->getLieu() : "";
		$query["lieu"]["lieuExact"] = $this->lieu->getLieuExact();
		$query["lieu"]["distance"]= $this->lieu->getDistance();
		$query["lieu"]["departement"]= $this->lieu->getDepartement();
		$query["lieu"]["coord"]= $this->lieu->getRawCoord();
		$query["lieu"]["bbox"]= $this->lieu->getRawBbox();
		$query["monnaie"] = $this->monnaie;
		$query["prixMin"] = $this->prixMin;
		$query["prixMax"] = $this->prixMax;
		$query["tailleEnfant"] = $this->tailleEnfant;
		$query["taille"] = $this->taille;
		$query["typeContrat"] = $this->typeContrat;
		$query["experienceMax"] = $this->experienceMax;
		$query["remunerationMin"] = $this->remunerationMin;
		$query["dureeMin"] = $this->dureeMin;
		$query["dureeMax"] = $this->dureeMax;
		$query["surfaceMin"] = $this->surfaceMin;
		$query["surfaceMax"] = $this->surfaceMax;
		$query["nbPiecesMin"] = $this->nbPiecesMin;
		$query["nbPiecesMax"] = $this->nbPiecesMax;
		$query["meuble"] = $this->meuble;
		$query["capacite"] = $this->capacite;
		$query["marque"] = $this->marque;
		$query["modele"] = $this->modele;
		$query["anneeMin"] = $this->anneeMin;
		$query["kilometrageMax"] = $this->kilometrageMax;
		$query["boiteVitesse"] = $this->boiteVitesse;
		$query["nb_resultats"] = $this->nb_resultats;

		return http_build_query($query);
	}

	/**
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{
		if ($this->recherche == null && $this->lieu->getLieu() == null && $this->categorie == null)
		{
			$context->buildViolation('recherche.form.errors.empty')
				->addViolation();
		}
	}

}

