<?php

namespace AnnoncesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Photo
 *
 * @ORM\Table(name="photo")
 * @ORM\Entity(repositoryClass="AnnoncesBundle\Repository\PhotoRepository")
 */
class Photo
{

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(name="photos", type="text")
	 */
	private $photos;

	/**
	 * @return string
	 */
	public function getPhotos()
	{
		return $this->photos;
	}

	/**
	 * @param string $photosPaths
	 */
	public function setPhotos($photosPaths)
	{
		$this->photos = $photosPaths;
	}


	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{
		$this->photos;
	}

}

