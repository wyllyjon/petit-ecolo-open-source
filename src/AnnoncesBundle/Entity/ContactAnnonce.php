<?php

namespace AnnoncesBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ContactAnnonce
{
	private $nom;
	private $mail;

	private $message;

	// Champ ne correspondant à rien, ne devant pas être rempli, mais à destination des bots
	// Si ce champ est rempli (alors qu'on va le cacher), c'est que c'est un robot qui a rempli le formulaire

	/**
	 * @Assert\Blank()
	 */
	private $spsd;

	private $annonce;

	/**
	 * @return mixed
	 */
	public function getNom()
	{
		return $this->nom;
	}

	/**
	 * @param mixed $nom
	 */
	public function setNom($nom)
	{
		$this->nom = $nom;
	}

	/**
	 * @return mixed
	 */
	public function getMail()
	{
		return $this->mail;
	}

	/**
	 * @param mixed $mail
	 */
	public function setMail($mail)
	{
		$this->mail = $mail;
	}

	/**
	 * @return mixed
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param mixed $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @return mixed
	 */
	public function getAnnonce()
	{
		return $this->annonce;
	}

	/**
	 * @param mixed $annonce
	 */
	public function setAnnonce($annonce)
	{
		$this->annonce = $annonce;
	}

	/**
	 * @return mixed
	 */
	public function getSpsd()
	{
		return $this->spsd;
	}

	/**
	 * @param mixed $spsd
	 */
	public function setSpsd($spsd): void
	{
		$this->spsd = $spsd;
	}




}

