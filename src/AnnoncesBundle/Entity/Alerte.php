<?php

namespace AnnoncesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Alerte
 *
 * @ORM\Table(name="alerte")
 * @ORM\Entity(repositoryClass="AnnoncesBundle\Repository\AlerteRepository")
 */
class Alerte
{

	public static $FREQUENCE_QUOTIDIEN = 'quotidien';
	public static $FREQUENCE_HEBDOMADAIRE = 'hebdomadaire';


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
	 *
	 * @Assert\Valid
	 *
	 **/
	private $user;


	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date_creation", type="date")
	 */
	private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_envoi", type="date")
     */
    private $dateDernierEnvoi;

    /**
     * @var string
     *
     * @ORM\Column(name="frequence", type="string", length=255)
     */
    private $frequence;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="titre", type="string", length=255)
	 */
	private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="recherche", type="string", length=2000)
     */
    private $recherche;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     *
     * @return Alerte
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

	/**
	 * @return string
	 */
	public function getTitre()
	{
		return $this->titre;
	}

	/**
	 * @param string $titre
	 */
	public function setTitre($titre)
	{
		$this->titre = $titre;
	}



    /**
     * Set dateEnvoi
     *
     * @param \DateTime $dateDernierEnvoi
     *
     * @return Alerte
     */
    public function setDateDernierEnvoi($dateDernierEnvoi)
    {
        $this->dateDernierEnvoi = $dateDernierEnvoi;

        return $this;
    }

    /**
     * Get dateEnvoi
     *
     * @return \DateTime
     */
    public function getDateDernierEnvoi()
    {
        return $this->dateDernierEnvoi;
    }

	/**
	 * @return \DateTime
	 */
	public function getDateCreation()
	{
		return $this->dateCreation;
	}

	/**
	 * @param \DateTime $dateCreation
	 */
	public function setDateCreation($dateCreation)
	{
		$this->dateCreation = $dateCreation;
	}



    /**
     * Set frequence
     *
     * @param string $frequence
     *
     * @return Alerte
     */
    public function setFrequence($frequence)
    {
        $this->frequence = $frequence;

        return $this;
    }

    /**
     * Get frequence
     *
     * @return string
     */
    public function getFrequence()
    {
        return $this->frequence;
    }



    /**
     * Set recherche
     *
     * @param string $recherche
     *
     * @return Alerte
     */
    public function setRecherche($recherche)
    {
        $this->recherche = $recherche;

        return $this;
    }

    /**
     * Get recherche
     *
     * @return string
     */
    public function getRecherche()
    {
        return $this->recherche;
    }
}

