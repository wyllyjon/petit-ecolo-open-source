<?php

namespace AnnoncesBundle\Controller;

use AnnoncesBundle\Entity\Annonce;
use AnnoncesBundle\Entity\Categorie;
use AnnoncesBundle\Entity\Recherche;
use AppBundle\Entity\AnonymousUser;
use AppBundle\Entity\User;
use AppBundle\Services\UrlHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Annonce controller.
 *
 * @Route("admin/annonces")
 */
class AdminAnnonceController extends Controller
{

	/**
	 * Valide une annonce
	 *
	 * @Route("/valider/{id}", name="ad_valide")
	 * @Method({"GET", "POST"})
	 */
	public function valideAnnonce (Annonce $annonce)
	{
		$em = $this->getDoctrine()->getManager();
		$annonce->setValidee(true);

		$em->flush();

		$mailSender = $this->container->get('AppBundle\Services\MailSender');

		if ($annonce->getAnonymousUser())
		{
			$mailSender->annonceValidee($annonce->getAnonymousUser(), $annonce);
		}
		else
		{
			$mailSender->annonceValidee($annonce->getUser(), $annonce);
		}

		return $this->redirectToRoute('sonata_admin_dashboard');
	}

	/**
	 * Refuse une annonce
	 *
	 * @Route("/refuser/{id}", name="ad_refuse")
	 * @Method({"GET", "POST"})
	 */
	public function refuseAnnonce (Request $request, Annonce $annonce)
	{
		$raison = $request->get('raison');

		$em = $this->getDoctrine()->getManager();
		$annonce->setValidee(false);
		$annonce->setRefusee(true);

		$em->flush();

		$mailSender = $this->container->get('AppBundle\Services\MailSender');

		$mailSender->annonceRefusee($annonce, $raison);

		return $this->redirectToRoute('sonata_admin_dashboard');
	}

	/**
	 * Renvoi un mail à l'utilisateur d'une annonce non confirmée
	 *
	 * @Route("/envoi_confirmation/{id}", name="ad_send_confirmation_mail")
	 * @Method({"GET", "POST"})
	 */
	public function sendMailConfirmationAction (Annonce $annonce)
	{
		$mailSender = $this->container->get('AppBundle\Services\MailSender');

		if ($annonce->getAnonymousUser())
		{
			$destinataire = $annonce->getAnonymousUser();
		}
		else
		{
			$destinataire = $annonce->getUser();
		}

		$mailSender->sendAnnonceNonConfirmee($destinataire->getNom(), $destinataire->getEmail(), $annonce->getId(), $annonce->getComplexId());
		$annonce->setDateRenvoiMailConfirmationCreation(new \DateTime());

		$this->getDoctrine()->getManager()->flush();

		return $this->redirectToRoute('sonata_admin_dashboard');
	}

	/**
	 * Invalide une annonce après signalement
	 *
	 * @Route("/invalider/{id}", name="ad_invalide")
	 * @Method({"GET", "POST"})
	 */
	public function invalideAnnonce (Request $request, Annonce $annonce)
	{
		$raison = $request->get('raison');

		$em = $this->getDoctrine()->getManager();
		$annonce->setValidee(false);
		$annonce->setRefusee(true);

		$em->flush();

		$mailSender = $this->container->get('AppBundle\Services\MailSender');

		if ($annonce->getAnonymousUser())
		{
			$mailSender->annonceInvalidee($raison, $annonce->getAnonymousUser(), $annonce);
		}
		else
		{
			$mailSender->annonceInvalidee($raison, $annonce->getUser(), $annonce);
		}

		return $this->redirectToRoute('sonata_admin_dashboard');
	}
}

