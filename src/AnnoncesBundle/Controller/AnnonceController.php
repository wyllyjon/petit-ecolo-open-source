<?php

namespace AnnoncesBundle\Controller;

use AnnoncesBundle\Entity\Annonce;
use AnnoncesBundle\Entity\Categorie;
use AnnoncesBundle\Entity\Photo;
use AnnoncesBundle\Entity\Recherche;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Annonce controller.
 *
 * @Route("annonces")
 */
class AnnonceController extends Controller
{

	private $statLogger;

	private function getStatLogger()
	{
		if ($this->statLogger == null)
		{
			$this->statLogger = $this->get("monolog.logger.stats");
		}
		return $this->statLogger;
	}

	/**
     * Lists all annonce entities.
     *
     * @Route("/", name="ad_index")
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $requete)
    {
        $em = $this->getDoctrine()->getManager();

		$repoCategorie = $em->getRepository('AnnoncesBundle:Categorie');
		$rootCategories = $repoCategorie->findRootCategories();

		foreach ($rootCategories as $rootCategory)
		{
			$rootCategory->setEnfants($repoCategorie->findByParent($rootCategory));
		}

		$recherche = new Recherche();
		$search_form = $this->createForm('AnnoncesBundle\Form\RechercheType', $recherche);
		$search_form->handleRequest($requete);

		if ($search_form->isSubmitted() && $search_form->isValid())
		{
			if ($recherche->getLieu()->getType() == 'postcode')
			{
				// On récupère un code postal du type '31000, Toulouse', on ne veut que '31000';
				$recherche->getLieu()->setLieu(explode(',', $recherche->getLieu()->getLieu())[0]);
			}


        	$logStat = $this->getStatLogger();
			$t0 = (new \DateTime())->getTimestamp();
			$annonces = $em->getRepository('AnnoncesBundle:Annonce')->findPaginated($recherche);
			$t1 = (new \DateTime())->getTimestamp();
			$logStat->debug("recheche indexAction : " . ($t1 - $t0) . "s.");
			$logStat->debug("Pour la recherche : ".serialize($recherche));

			$encoders = array(new JsonEncoder());
			$normalizer = new ObjectNormalizer();
			$normalizer->setIgnoredAttributes(array('parent', 'enfants'));
			$normalizer->setCircularReferenceHandler(function ($object) {
				return $object->getCode();
			});
			$normalizers = array($normalizer);
			$serializer = new Serializer($normalizers, $encoders);
			$rechercheJson= $serializer->serialize($recherche, 'json');

			return $this->render('annonce/list.html.twig', array(
				'annonces' => $annonces,
				'page' => $recherche->getPage(),
//				'nb_resultats' => $recherche->getNbResultats(),
				'nb_page' => ceil($annonces->count() /  $recherche->getNbResultats()),
				'categories' => $rootCategories,
				'search_form' => $search_form->createView(),
				'test' => $recherche->getQueryString(),
				'recherche_json' => $rechercheJson,
				'categorieAnnonce' => $recherche->getCategorie() != null ? $recherche->getCategorie()->getNom() : "",
			));
		}

		return $this->render('annonce/list.html.twig', array(
			'categories' => $rootCategories,
			'search_form' => $search_form->createView()
		));

    }

	/**
	 * Lists the last annonces
	 *
	 * @Route("/dernieres", name="ad_last")
	 * @Method({"GET","POST"})
	 */
	public function lastAction(Request $requete)
	{
		$em = $this->getDoctrine()->getManager();
		$page = $requete->query->has('page') ? $requete->query->get('page') : 1;

		$nb_resultats = 12;

		$annonces = $em->getRepository('AnnoncesBundle:Annonce')->findLast($nb_resultats, $page);

		return $this->render('annonce/dernieres.html.twig', array(
			'annonces' => $annonces,
			'page' => $page,
			'nb_page' => ceil($annonces->count() /  $nb_resultats)
		));

	}

	/**
	 * Liste les annonces de l'utilisateur.
	 *
	 * @Route("/favorites", name="annonces_favorites")
	 * @Method({"GET","POST"})
	 */
	public function mesAnnoncesAction(Request $requete)
	{
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			throw new AccessDeniedException();
		}

		$user = $this->get('security.token_storage')->getToken()->getUser();

		$em = $this->getDoctrine()->getManager();


		$annonces = $em->getRepository('AnnoncesBundle:Annonce')->mesAnnoncesFavorites($user);


		return $this->render(':user:mes_annonces.html.twig', array(
			'favorites' => true,
			'annonces' => $annonces
		));

	}


	/**
	 * Liste les annonces favorites de l'utilisateur.
	 *
	 * @Route("/mes-annonces", name="mes_annonces")
	 * @Method({"GET","POST"})
	 */
	public function mesAnnoncesFavoritesAction(Request $requete)
	{
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			throw new AccessDeniedException();
		}
		$user = $this->get('security.token_storage')->getToken()->getUser();

		$em = $this->getDoctrine()->getManager();


		$annonces = $em->getRepository('AnnoncesBundle:Annonce')->mesAnnonces($user);


		return $this->render(':user:mes_annonces.html.twig', array(
			'annonces' => $annonces
		));

	}

	/**
	 * Lists all annonce entities from specified category.
	 *
	 * @Route("/categorie/{id}", name="ad_category")
	 * @Method("GET")
	 */
	public function categoryAction(Categorie $cat)
	{
		$em = $this->getDoctrine()->getManager();

		$annonces = $em->getRepository('AnnoncesBundle:Annonce')->findAllByCategory($cat);

		return $this->render('annonce/list.html.twig', array(
			'annonces' => $annonces,
		));
	}

    /**
     * Creates a new annonce entity.
     *
     * @Route("/nouvelle", name="ad_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $annonce = new Annonce();

		if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			$annonce->setUser($this->getUser());
		}

		$em = $this->getDoctrine()->getManager();
		$repoCategorie = $em->getRepository('AnnoncesBundle:Categorie');
		$rootCategories = $repoCategorie->findRootCategories();

        $form = $this->createForm('AnnoncesBundle\Form\AnnonceType', $annonce);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $annonce->setComplexId(uniqid('', true));
            $annonce->setDateCreation(new \DateTime("now"));
            $annonce->setDateModification(new \DateTime("now"));
			$annonce->setDateRecherche(new \DateTime("now"));
			$annonce->setJourRemontee(date('w'));

			$em->persist($annonce);
			$em->flush();

			// On traite les image
			$this->gerePhotos($annonce);


			$stat = $em->getRepository("AppBundle:Stats")->find(1);
			$stat->setNbAnnonces($stat->getNbAnnonces() + 1);

            $em->flush();

			$urlHelper = $this->container->get('AppBundle\Services\UrlHelper');
		   	return $this->redirectToRoute('ad_confirme', array('id' => $annonce->getId(), 'complexId' => $annonce->getComplexId()));
        }

        return $this->render('annonce/new.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
			'categories' => $rootCategories
        ));
    }

	/**
	 * @param $annonce
	 */
    private function gerePhotos(Annonce $annonce)
	{
		$photos  = $annonce->getPhotos();
		if ($photos != null && $photos != "")
		{
			$newPaths = array();
			$paths = explode(";", $photos);
			$imagesHelper = $this->container->get('AppBundle\Services\ImagesHelper');
			foreach ($paths as $path)
			{
				$newPath = $imagesHelper->movePhoto($annonce->getId(), $path);
				if ($newPath != null)
				{
					array_push($newPaths, $newPath);
				}
			}
			$annonce->setPhotos(implode(";", $newPaths));
		}
	}

	/**
	 * Finds and displays a annonce entity.
	 *
	 * @Route("/fin-creation/{id}/{complexId}", name="ad_fin_creation")
	 * @Method("GET")
	 */
    public function endCreationAction(Request $request, Annonce $annonce)
	{
		$complexId = $request->get('complexId');
		$this->verifycomplexIdOrUser($complexId, $annonce);

		$mailSuccess = $this->confirmSendMail($request, $annonce);

		return $this->render('annonce/attente_moderation.html.twig', array(
			'annonce' => $annonce,
			'mail_success' => $mailSuccess
		));

	}

	/**
	 * Est appelé après que le paiement soit fait côté client
	 *
	 * @Route("/paiement-realise/{id}/{complexId}", name="ad_payment_done")
	 * @Method("GET")
	 */
	public function paiementRecuAction(Request $request, Annonce $annonce)
	{
		$complexId = $request->get('complexId');
		$this->verifycomplexIdOrUser($complexId, $annonce);

		//TODO: Faire l'action
	}

	private function verifycomplexIdOrUser($complexId, Annonce $annonce)
	{
		if ($complexId != null)
		{
			if ($complexId != $annonce->getComplexId())
			{
				throw new AccessDeniedException();
			}
		}
		elseif ($annonce->getUser() && $annonce->getUser() != $this->get('security.token_storage')->getToken()->getUser())
		{
			throw new AccessDeniedException();
		}
	}

	private function confirmSendMail(Request $request, Annonce $annonce)
	{
		$complexId = $request->get('complexId');
		$this->verifycomplexIdOrUser($complexId, $annonce);

		$em = $this->getDoctrine()->getManager();
		$annonce->setConfirme(true);

		$em->flush();

		$mailSender = $this->container->get('AppBundle\Services\MailSender');

		if ($destinataire = $annonce->getUser())
		{
			$mail = $destinataire->getEmail();
			$nom = $destinataire->getPrenom();
		}
		else{
			$destinataire = $annonce->getAnonymousUser();
			$mail = $destinataire->getEmail();
			$nom = $destinataire->getNom();
		}
//		$destinataire = $annonce->getUser() ? $annonce->getUser() : $annonce->getAnonymousUser();

		return $mailSender->sendAnnonceAttenteModeration($nom, $mail, $annonce->getComplexId());
	}

	/**
	 * Finds and displays a annonce entity.
	 *
	 * @Route("/confirmer/{id}/{complexId}", name="ad_confirme")
	 * @Method("GET")
	 */
	public function confirmCreationAction(Request $request, Annonce $annonce)
	{
		$complexId = $request->get('complexId');
		$this->verifycomplexIdOrUser($complexId, $annonce);

		$deleteForm = $this->createDeleteForm($annonce, $complexId);
		$em = $this->getDoctrine()->getManager();
//		$repoPhoto = $em->getRepository('AnnoncesBundle:Photo');
//		$photos = $repoPhoto->findAllByAnnonce($annonce);

		$repoCategorie = $em->getRepository('AnnoncesBundle:Categorie');
		$catCode = $annonce->getCategorie()->getCode();
		if($catCode)
		{
			$cat = $repoCategorie->findByCode($catCode);
			$annonce->setCategorie($cat);
		}

		if ($annonce->getAnonymousUser())
		{
			$annonce->setUser($annonce->getAnonymousUser());
		}


		$options = array(
			'annonce' => $annonce,
			'delete_form' => $deleteForm->createView(),
			'confirmation' => true
		);

		return $this->render('annonce/confirm.html.twig', $options);
	}


	/**
	 * Displays a form to edit an existing annonce after confirmation.
	 *
	 * @Route("/confirmer/editer/{id}/{complexId}", name="ad_confirm_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAfterConfirmationAction (Request $request, Annonce $annonce)
	{
		return $this->editContent($request, $annonce, true);
	}


	/**
	 * Displays a form to edit an existing annonce entity.
	 *
	 * @Route("/editer/{id}/{complexId}", name="ad_edit", defaults={"complexId" = null})
	 * @Method({"GET", "POST"})
	 */
	public function editAction(Request $request, Annonce $annonce)
	{
		return $this->editContent($request, $annonce, false);
	}


	private function editContent(Request $request, Annonce $annonce, $fromConfirmation)
	{
		$complexId = $request->get('complexId');
		$this->verifycomplexIdOrUser($complexId, $annonce);

		// TODO :  Pourquoi cette ligne ? Utile ?
		$annonce->setPhotos($annonce->getPhotos());


		$em = $this->getDoctrine()->getManager();
		$repoCategorie = $em->getRepository('AnnoncesBundle:Categorie');
		$rootCategories = $repoCategorie->findRootCategories();

		foreach ($rootCategories as $rootCategory)
		{
			$rootCategory->setEnfants($repoCategorie->findByParent($rootCategory));
		}

		$editForm = $this->createForm('AnnoncesBundle\Form\AnnonceType', $annonce);
		$oldOptions = $this->getOptions($annonce);
		$editForm->handleRequest($request);

		if ($editForm->isSubmitted() && $editForm->isValid()) {
			$annonce->setDateModification(new \DateTime("now"));
			$annonce->setRefusee(false);

			// Si la personne vient de sélectionner le pack remontée
			// OU si il n'y avait plus de remontées et qu'elle en prend de nouvelles
			if ((!$oldOptions["packRemontee"] && $annonce->hasPackRemontee()) || ($oldOptions["nbRemonteesRestantes"] == 0 && $annonce->getNbRemonteesRestantes() > 0))
			{
				// On défini le jour de remontée à aujourd'hui
				$annonce->setJourRemontee(date("w"));
			}

			$em = $this->getDoctrine()->getManager();

			// On traite les image
			$this->gerePhotos($annonce);

			$annonce->setConfirme(false);
			$annonce->setValidee(false);

			$em->flush();

			$urlHelper = $this->container->get('AppBundle\Services\UrlHelper');

			return $this->redirectToRoute('ad_confirme', array('id' => $annonce->getId(), 'complexId' => $annonce->getComplexId()));

		}

		return $this->render('annonce/edit.html.twig', array(
			'annonce' => $annonce,
			'categorieAnnonce' => $annonce->getCategorie()->getNom(),
			'form' => $editForm->createView(),
			'categories' => $rootCategories,
		));
	}

	/**
	 * get old options to know if options in the edited annonce is new or not
	 */
	private function getOptions(Annonce $annonce)
	{
		$options = [];
		$options["packUrgent"] = $annonce->hasPackUrgent();
		$options["packRemontee"] = $annonce->hasPackRemontee();
		$options["frequenceRemontee"] = $annonce->getFrequenceRemontee();
		$options["nbRemonteesRestantes"] = $annonce->getNbRemonteesRestantes();

		return $options;
	}


	/**
     * Finds and displays a annonce entity.
     *
     * @Route("/voir/{id}/{titre}/{complexId}", name="ad_show", defaults={"complexId" = null})
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Annonce $annonce = null)
    {

		if ($annonce == null)
		{
			// L'annonce n'existe pas, on redirige vers une page d'erreur spécifique
			return $this->render('annonce/error.html.twig');
		}

    	$complexId = $request->get('complexId');
    	if ($complexId == null && (!$annonce->isConfirme() || !$annonce->isValidee()))
		{
			return $this->render('statiques/annonce_attente.twig');
		}

		if ($annonce->getAnonymousUser())
		{
			$annonce->setUser($annonce->getAnonymousUser());
		}


		if (($annonce->getUser() && $annonce->getUser() == $this->get('security.token_storage')->getToken()->getUser()) ||
			($complexId != null && $complexId === $annonce->getComplexId()))
		{
			$isOwnByCurrentUser = true;
		}
		else
		{
			$isOwnByCurrentUser = false;
		}

        $deleteForm = $this->createDeleteForm($annonce, $complexId);
		$em = $this->getDoctrine()->getManager();
//		$repoPhoto = $em->getRepository('AnnoncesBundle:Photo');
//		$photos = $repoPhoto->findAllByAnnonce($annonce);

		$repoCategorie = $em->getRepository('AnnoncesBundle:Categorie');
		$catCode = $annonce->getCategorie()->getCode();
		if($catCode)
		{
			$cat = $repoCategorie->findByCode($catCode);
			$annonce->setCategorie($cat);
		}

		$contactForm = $this->createForm('AnnoncesBundle\Form\ContactAnnonceType');

		$contactForm->handleRequest($request);

		if ($contactForm->isSubmitted() && $contactForm->isValid()) {

			$contactData = $contactForm->getData();
			$mailSender = $this->container->get('AppBundle\Services\MailSender');

			$destinataire = $annonce->getUser() ? $annonce->getUser() : $annonce->getAnonymousUser();

			$success = $mailSender->sendContactAnnonce($contactData->getNom(), $contactData->getMail(), $contactData->getMessage(), $destinataire, $annonce);

			return $this->render('annonce/show.html.twig', array(
				'annonce' => $annonce,
//				'photos' => $photos,
				'isOwnByCurrentUser' => $isOwnByCurrentUser,
				'contact_form' => $contactForm->createView(),
				'delete_form' => $deleteForm->createView(),
				'mail_success' => $success
			));

		}

		$options = array(
			'annonce' => $annonce,
//			'photos' => $photos,
			'isOwnByCurrentUser' => $isOwnByCurrentUser,
			'contact_form' => $contactForm->createView(),
			'delete_form' => $deleteForm->createView(),
		);

		if ($complexId != null)
		{
			$options["complexId"] = $complexId;
		}

        return $this->render('annonce/show.html.twig', $options);
    }


    /**
     * Deletes a annonce entity.
     *
     * @Route("/{id}", name="ad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Annonce $annonce)
    {
		$complexId = $request->get('complexId');
		$this->verifycomplexIdOrUser($complexId, $annonce);

        $form = $this->createDeleteForm($annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	if ($form->getData()["complexId"] != $annonce->getComplexId())
			{
				throw new AccessDeniedException();
			}
            $em = $this->getDoctrine()->getManager();
            $em->remove($annonce);
            $em->flush();
        }

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
        	return $this->redirectToRoute('mes_annonces');
		}
        return $this->redirectToRoute('homepage');
    }

    /**
     * Creates a form to delete a annonce entity.
     *
     * @param Annonce $annonce The annonce entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Annonce $annonce, $complexId = null)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ad_delete', array('id' => $annonce->getId())))
            ->setMethod('DELETE')
            ->getForm()
			->add("complexId", HiddenType::class, array('data' => $complexId))
        ;

    }

	/**
	 * Add to favorites.
	 *
	 * @Route("/favoris/add/{id}", name="add_favorite")
	 *@Method("POST")
	 */
    public function addToFavorites(Annonce $annonce)
	{
		// on teste si c'est bien une requete ajax
//		if ($request->isXMLHttpRequest()) {


			if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
			{
				return new JsonResponse(array('result' => 'error', 'message' => 'Vous devez être connecté pour pouvoir enregistrer une annonce en favoris'));
			}

			$user = $this->get('security.token_storage')->getToken()->getUser();
			$user->addAnnonceFavorite($annonce);
			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();

			return new JsonResponse(array('result' => 'ok'));
//		}
	}

	/**
	 * Add to favorites.
	 *
	 * @Route("/favoris/remove/{id}", name="remove_favorite")
	 *@Method("POST")
	 */
	public function removeToFavorites(Annonce $annonce)
	{
		// on teste si c'est bien une requete ajax
//		if ($request->isXMLHttpRequest()) {


		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			return new JsonResponse(array('result' => 'error', 'message' => 'Vous devez être connecté pour pouvoir supprimer une annonce de vos favoris'));
		}

		$user = $this->get('security.token_storage')->getToken()->getUser();
		$user->removeAnnonceFavorite($annonce);
		$em = $this->getDoctrine()->getManager();
		$em->persist($user);
		$em->flush();

		return new JsonResponse(array('result' => 'ok'));
//		}
	}

	/**
	 * Add to favorites.
	 *
	 * @Route("/signaler/{id}", name="add_signale")
	 *@Method("POST")
	 */
	public function addToSignalees(Annonce $annonce, Request $request)
	{
		// on teste si c'est bien une requete ajax
//		if ($request->isXMLHttpRequest()) {
		$raison = $request->get("raison");

		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			return new JsonResponse(array('result' => 'error', 'message' => 'Vous devez être connecté pour pouvoir signaler une annonce'));
		}

		$user = $this->get('security.token_storage')->getToken()->getUser();
		$user->addAnnonceSignalee($annonce);
		$em = $this->getDoctrine()->getManager();
		$em->persist($user);
		$em->flush();

		return new JsonResponse(array('result' => 'ok'));
//		}
	}

	/**
	 * Execute les remontées automatiques quotidiennes
	 *
	 * @Route("/executeRemonteesAutoQuotidiennes", name="execute_remontees_jour")
	 *@Method("GET")
	 */
	public function gereRemonteesAutomatiquesQuotidiennes()
	{
		$result = $this->gereRemonteesAuto(true);
		return new JsonResponse(array('result' => $result));
	}


	/**
	 * Execute les remontées automatiques hebdomadaires
	 *
	 * @Route("/executeRemonteesAutoHebdomadaires", name="execute_remontees_semaine")
	 *@Method("GET")
	 */
	public function gereRemonteesAutomatiquesHebdomadaires()
	{
		$result = $this->gereRemonteesAuto(false);
		return new JsonResponse(array('result' => $result));
	}

	private function gereRemonteesAuto($quotidien)
	{
		$em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository('AnnoncesBundle:Annonce');

		// On récupère les annonces qui doivent remonter quotidiennement ou hebdomadairement
		$annonces = $repo->findRemonteesAuto($quotidien);

		foreach ($annonces as $annonce)
		{
			$annonce->setNbRemonteesRestantes($annonce->getNbRemonteesRestantes() - 1);
			$annonce->setDateRecherche(new \DateTime("now"));

			try {

				$em->flush();
			}
			catch (ORMException $e)
			{
				$logger = $this->get('logger');
				$logger->error("Une erreur est survenue lors de la modification des annonces pour les remontées auto", $e);
				return 'ko';
			}
			return 'ok';
		}
		return 'no-result';

	}
}

