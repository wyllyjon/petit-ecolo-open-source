<?php

namespace AnnoncesBundle\Controller;

use AnnoncesBundle\Entity\Alerte;
use AnnoncesBundle\Entity\LieuRecherche;
use AnnoncesBundle\Entity\Recherche;
use AnnoncesBundle\Helper\SerializationPropertyTypeExtractor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Annonce controller.
 *
 * @Route("alertes")
 */
class AlerteController extends Controller
{

	/**
	 * Ajoute une alerte.
	 *
	 * @Route("/ajouter", name="add_alerte")
	 *	@Method("POST")
	 */
    public function ajouteAlerteAction(Request $request)
	{
		$rechercheJson = $request->get('recherche');
		$frequence = $request->get('frequence');
		$titre = $request->get('titre');

		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			return new JsonResponse(array('result' => 'error', 'message' => 'Vous devez être connecté pour pouvoir enregistrer une alerte'));
		}

		$user = $this->get('security.token_storage')->getToken()->getUser();
		$alerte = new Alerte();
		$alerte->setUser($user);
		$alerte->setTitre($titre);
		$alerte->setRecherche($rechercheJson);
		$alerte->setFrequence($frequence === Alerte::$FREQUENCE_HEBDOMADAIRE ? Alerte::$FREQUENCE_HEBDOMADAIRE : Alerte::$FREQUENCE_QUOTIDIEN);
		$alerte->setDateCreation(new \DateTime("now"));
		$alerte->setDateDernierEnvoi(new \DateTime("now"));

		$em = $this->getDoctrine()->getManager();
		$em->persist($alerte);
		$em->flush();

		return new JsonResponse(array('result' => 'ok'));
	}

	/**
	 * Supprime une alerte.
	 *
	 * @Route("/supprimer/{id}", name="remove_alerte")
	 *	@Method("POST")
	 */
	public function supprimeAlerteAction(Alerte $alerte)
	{
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			return new JsonResponse(array('result' => 'error', 'message' => 'Vous devez être connecté pour pouvoir supprimer une annonce de vos favoris'));
		}

		$em = $this->getDoctrine()->getManager();
		$em->remove($alerte);
		$em->flush();

		return new JsonResponse(array('result' => 'ok'));
//		}
	}

	/**
	 * Liste les alertes de l'utilisateur.
	 *
	 * @Route("/mes-alertes", name="mes_alertes")
	 * @Method({"GET","POST"})
	 */
	public function mesAlertesAction(Request $requete)
	{
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			throw new AccessDeniedException();
		}
		$user = $this->get('security.token_storage')->getToken()->getUser();

		$em = $this->getDoctrine()->getManager();


		$alertes = $em->getRepository('AnnoncesBundle:Alerte')->findMesAlertes($user);


		return $this->render(':user:mes_alertes.html.twig', array(
			'alertes' => $alertes
		));

	}

	/**
	 * On fait une URL pour être appellée par un webcron (car il doit appeler une URL et pas un commande de la console)
	 * On configure une restriction d'IP dans le fichier security.yml pour qu'elle ne soit appelée que par localhost
	 *
	 * @Route("/envoi/{type}", name="envoie_alerte")
	 */
	public function envoiAlertes(Request $request)
	{
		$type = $request->get('type');

		$em = $this->getDoctrine()->getManager();
		if ($type == Alerte::$FREQUENCE_QUOTIDIEN)
		{
			$alertes = $em->getRepository('AnnoncesBundle:Alerte')->findAlertesQuotidiennes();
		}
		else if($type == Alerte::$FREQUENCE_HEBDOMADAIRE)
		{
			$alertes = $em->getRepository('AnnoncesBundle:Alerte')->findAlertesHebdomadaires();
		}
		else
		{
			return $this->render('alertes_result.html.twig', array(
				'erreur' =>  "Cette url n'existe pas",
			));
		}

		$mailSender = $this->container->get('AppBundle\Services\MailSender');

		foreach($alertes as $alerte)
		{
			$user = $alerte->getUser();
			$recherche_json = $alerte->getRecherche();
			$dateDernierEnvoi = $alerte->getDateDernierEnvoi();

			$encoders = array(new JsonEncoder());
			$normalizer = new ObjectNormalizer(null, null, null, new SerializationPropertyTypeExtractor());
			$normalizer->setIgnoredAttributes(array('parent', 'enfants'));
			$normalizer->setCircularReferenceHandler(function ($object) {
				return $object->getCode();
			});
			$normalizers = array($normalizer,  new ArrayDenormalizer());
			$serializer = new Serializer($normalizers, $encoders);
			$recherche = $serializer->deserialize($recherche_json, Recherche::class, 'json');


			// On garde la méthode findPaginated car comme c'est paginé, on ne récupère que les 10 premiers éléments et on a le count accessible.
			$annonces = $em->getRepository('AnnoncesBundle:Annonce')->findPaginated($recherche, $dateDernierEnvoi);


			if (count($annonces) > 0 )
			{
				$mailSender->sendAlertes($user, $annonces, $alerte->getTitre(), $recherche->getQueryString());
			}

			$alerte->setDateDernierEnvoi(new \DateTime("now"));
			$em->flush();
		}



		return $this->render('alertes_result.html.twig', array(
			'nbAlertes' =>  count($alertes),
		));
	}

}

