<?php

namespace AnnoncesBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CategorieAdmin extends AbstractAdmin
{

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper->addIdentifier('positionAffichage');
		$listMapper->addIdentifier('nom');
		$listMapper->addIdentifier('parent')->add('parent.nom');
	}

	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper->add('nom', 'text');
		$formMapper->add('code');
		$formMapper->add('parent', 'entity', array(
			'class' => 'AnnoncesBundle\Entity\Categorie',
			'choice_label' => 'nom',
			'required' => false
		));
		$formMapper->add('positionAffichage');
	}
}