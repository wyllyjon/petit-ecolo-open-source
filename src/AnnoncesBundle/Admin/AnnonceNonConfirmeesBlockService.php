<?php

namespace AnnoncesBundle\Admin;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Monolog\Logger;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnonceNonConfirmeesBlockService extends AbstractBlockService
{
	private $manager;
	private $logger;

	/**
	 * @param string                   	$name
	 * @param EngineInterface          	$templating
	 * @param EntityManager				$manager
	 *
	 */
	public function __construct($name = null, EngineInterface $templating = null, EntityManager $manager, Logger $logger)
	{
		$this->manager = $manager;
		$this->logger = $logger;
		parent::__construct($name, $templating);
	}

	public function configureSettings(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'title'    => 'admin.annonces.non_validees',
			'template' => 'admin/annonces/non_confirmees.html.twig',
		));
	}


	public function execute(BlockContextInterface $blockContext, Response $response = null)
	{
		$erreur = null;
		try {
			$annonces = $this->manager->getRepository('AnnoncesBundle:Annonce')->findNonConfirmees();
		}
		catch (ORMException $e)
		{
			$erreur = "Erreur lors de la récupération des annonces non confirmées pour l'admin : ".$e;
			$this->logger->error($erreur);
		}

		return $this->renderResponse($blockContext->getTemplate(), array(
			'block'     => $blockContext->getBlock(),
			'annonces' => $annonces,
			'erreur' => $erreur,
			'settings' => $blockContext->getSettings(),
		), $response);
	}

}