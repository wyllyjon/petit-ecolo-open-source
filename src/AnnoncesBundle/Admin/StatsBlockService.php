<?php

namespace AnnoncesBundle\Admin;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Monolog\Logger;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StatsBlockService extends AbstractBlockService
{
	private $manager;
	private $logger;

	/**
	 * @param string                   	$name
	 * @param EngineInterface          	$templating
	 * @param EntityManager				$manager
	 *
	 */
	public function __construct($name = null, EngineInterface $templating = null, EntityManager $manager, Logger $logger)
	{
		$this->manager = $manager;
		$this->logger = $logger;
		parent::__construct($name, $templating);
	}

	public function configureSettings(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'title'    => 'admin.annonces.en_attente_validation',
			'template' => 'admin/stats.html.twig',
			'type' => 'utilisateurs',
			'css_class' => 'bg-blue'
		));
	}


	public function execute(BlockContextInterface $blockContext, Response $response = null)
	{
		$settings = $blockContext->getSettings();
		$type = $settings["type"];
		$nombre = "-";
		$texte = "-";
		$css = "bg-aqua";
		$iconCss = "";

		if ($type === 'utilisateurs')
		{
			try {
				$nombre = $this->manager->getRepository('AppBundle\Entity\User')->getNbUtilisateurs()['nombre'];
			}
			catch(ORMException $e)
			{
				$nombre = "ERREUR";
				$this->logger->error("Impossible de récupérer le nombre d'utilisateurs : ".$e);
			}
			$iconCss = "ion ion-person";
			$texte = "admin.stats.utilisateurs_titre";
			$css="bg-green";
			$listePath = "admin_app_user_list";
		}
		elseif($type === 'annonces')
		{
			try {
				$nombre = $this->manager->getRepository('AnnoncesBundle\Entity\Annonce')->getNbAnnonces()['nombre'];
			}
			catch(ORMException $e)
			{
				$nombre= "ERREUR";
				$this->logger->error("Impossible de récupérer le nombre d'annonces : ".$e);
			}
			$iconCss = "ion ion-document";
			$texte = "admin.stats.annonces_titre";
			$css = "bg-aqua";
			$listePath = "admin_annonces_annonce_list";
		}
		elseif($type === 'all-annonces')
		{
			try {
				$nombre = $this->manager->getRepository('AppBundle:Stats')->getStatNbAnnonces()['nb'];
			}
			catch(ORMException $e)
			{
				$nombre= "ERREUR";
				$this->logger->error("Impossible de récupérer le nombre d'annonces depuis le lancement : ".$e);
			}
			$iconCss = "ion ion-document";
			$texte = "admin.stats.all_annonces_titre";
			$css = "bg-purple";
			$listePath = "";
		}
		elseif($type === 'all-utilisateurs')
		{
			try {
				$nombre = $this->manager->getRepository('AppBundle:Stats')->getStatNbUtilisateurs()['nb'];
			}
			catch(ORMException $e)
			{
				$nombre= "ERREUR";
				$this->logger->error("Impossible de récupérer le nombre d'utilisateurs depuis le lancement : ".$e);
			}
			$iconCss = "ion ion-person";
			$texte = "admin.stats.all_utilisateurs_titre";
			$css = "bg-purple";
			$listePath = "";
		}

		return $this->renderResponse($blockContext->getTemplate(), array(
			'block'     => $blockContext->getBlock(),
			'nombre' => $nombre,
			'texte' => $texte,
			'css' => $css,
			'icon' => $iconCss,
			'footerPath' => $listePath
		), $response);
	}

}