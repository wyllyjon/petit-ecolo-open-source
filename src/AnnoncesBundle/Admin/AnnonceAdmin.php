<?php


namespace AnnoncesBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class AnnonceAdmin extends AbstractAdmin
{

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper->addIdentifier('titre');
		$listMapper->addIdentifier('categorie');
		$listMapper->addIdentifier('anonymousUser');
		$listMapper->addIdentifier('type');
		$listMapper->addIdentifier('prix');
		$listMapper->addIdentifier('monnaie');
		$listMapper->addIdentifier('confirme');
		$listMapper->addIdentifier('validee');
		$listMapper->addIdentifier('packPhoto');
		$listMapper->addIdentifier('packUrgent');
		$listMapper->addIdentifier('dateAlaune', 'date', array('format' => 'd/m/y'));
		$listMapper->addIdentifier('dateFinAlaune', 'date', array('format' => 'd/m/y'));
		$listMapper->addIdentifier('dateRemontee', 'date', array('format' => 'd/m/y'));
		$listMapper->addIdentifier('dateFinRemontee', 'date', array('format' => 'd/m/y'));
		$listMapper->addIdentifier('dateModification', 'date', array('format' => 'd/m/y'));
	}

	protected function configureFormFields(FormMapper $formMapper)
	{
		$formMapper->with('Données');
			$formMapper->add('titre', 'text');
			$formMapper->add('categorie', 'entity', array(
				'class' => 'AnnoncesBundle\Entity\Categorie',
				'choice_label' => 'nom'
			));
			$formMapper->add('type', 'text');
			$formMapper->add('prix', 'text');
			$formMapper->add('packPhoto', 'checkbox');
			$formMapper->add('packUrgent', 'checkbox');
		$formMapper->end()->end();

		$formMapper->with('A la une', [
			'class'       => 'col-md-6'])
					->add('dateAlaune', 'date')
					->add('dateFinAlaune', 'date')
					->end()->end();
		$formMapper->with('Remontée automatique', [
			'class'       => 'col-md-6'])
					->add('dateRemontee', 'date')
					->add('dateFinRemontee', 'date')
					->end()->end();
	}

	protected $datagridValues = array(
		'_sort_order' => 'DESC', // reverse order (default = 'ASC')
		'_sort_by' => 'dateModification'  // name of the ordered field
		// (default = the model's id field, if any)

		// the '_sort_by' key can be of the form 'mySubModel.mySubSubModel.myField'.
	);

	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->remove('edit');
		$collection->remove('create');
	}


}