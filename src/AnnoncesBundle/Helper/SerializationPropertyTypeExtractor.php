<?php
/**
 * Created by PhpStorm.
 * User: jon
 * Date: 11/04/18
 * Time: 17:35
 */

namespace AnnoncesBundle\Helper;


use AnnoncesBundle\Entity\Categorie;
use AnnoncesBundle\Entity\LieuRecherche;
use AnnoncesBundle\Entity\Recherche;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\PropertyInfo\Type;

class SerializationPropertyTypeExtractor implements PropertyTypeExtractorInterface {

	/**
	 * {@inheritdoc}
	 */
	public function getTypes($class, $property, array $context = array())
	{
		if (!is_a($class, Recherche::class, true)) {
			return null;
		}

		if ('lieu' === $property)
		{
			return [new Type(Type::BUILTIN_TYPE_OBJECT, true, LieuRecherche::class)];
		}
		if ('categorie' === $property)
		{
			return [new Type(Type::BUILTIN_TYPE_OBJECT, true, Categorie::class)];
		}

		return null;
	}
}