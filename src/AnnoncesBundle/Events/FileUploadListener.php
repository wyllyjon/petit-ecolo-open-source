<?php

namespace AnnoncesBundle\Events;


use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Liip\ImagineBundle\Controller\ImagineController;
use Monolog\Logger;
use Vich\UploaderBundle\Event\Event;

class FileUploadListener
{
	private $imagine;
	private $logger;

	public function __construct(ImagineController $imagineController, Logger $logger)
	{
		$this->imagine = $imagineController;
		$this->logger = $logger;
	}

	public function onVichUploaderPreUpload(Event $event)
	{
		$object = $event->getObject();
		$mapping = $event->getMapping();

		$imagine = new Imagine();
		$imageFile = $object->getImageFile();
		$tmpPath = $imageFile->getPathName();

		try {
			$imageSize = getimagesize($tmpPath);
			// [0] : width / [1] : height
			if ($imageSize[0] > 750 || $imageSize[1] > 600)
			{
				$ratio = $imageSize[0] / $imageSize[1];

				// Taille de l'image
				if ($ratio <= 1)
				{
					// Si image verticale, on fixe la hauteur max à 600
					$size = new Box(600 * $ratio, 600);
				}
				else
				{
					// Si image horizontale, on fixe la largeur max à 750
					$size = new Box(750, 750 / $ratio);
				}

				$imagine->open($tmpPath)->resize($size)->save($tmpPath, ['format' => 'jpeg']);
			}
		}
		catch (\Exception $e)
		{
			$this->logger->error("Impossible de récupérer la taille de l'image uploadée");
			throw new Exception("Erreur lors du traitement de l'image");
		}


	}
}