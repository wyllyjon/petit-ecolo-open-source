<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('address', TextType::class, array('label' => 'annonces.form.adresse_label', 'required' => false, 'attr' => array('class'=>'inputAdresse')))
				->add('postal', TextType::class, array('label' => 'annonces.form.postal_label', 'required' => true, 'attr' => array('class'=>'inputPostal', 'onblur' => 'verifAdresse()')))
				->add('ville', TextType::class, array('label' => 'annonces.form.ville_label', 'required' => true, 'attr' => array('class'=>'inputVille', 'onblur' => 'verifAdresse()')))
				->add('lat', HiddenType::class, array( 'required' => true, 'attr' => array('class'=>'inputLat')))
				->add('lng', HiddenType::class, array( 'required' => true, 'attr' => array('class'=>'inputLng')))
				->add('region', HiddenType::class, array( 'required' => false, 'attr' => array('class'=>'inputRegion')));

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_address';
    }


}
