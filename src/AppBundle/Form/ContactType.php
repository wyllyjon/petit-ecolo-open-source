<?php

namespace AppBundle\Form;


use AppBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('nom', TextType::class, array('label' => 'annonces.contact.nom_label'))
			->add('mail', EmailType::class, array('label' => 'annonces.contact.mail_label'))
			->add('sujet', ChoiceType::class, array(
				'choices' => array(
					'contact.sujet_probleme' => Contact::$SUJECT_BUG,
					'contact.sujet_amelioration' => Contact::$SUJECT_AMELIORATION,
					'contact.sujet_signaler' => Contact::$SUJECT_SIGNALER,
					'contact.sujet_publicite' => Contact::$SUJET_PUB,
					'contact.sujet_autre' => Contact::$SUJECT_AUTRE,
				),
			))
			->add('message', TextareaType::class)
			->add('spsd', TextType::class, array('required' => false, 'label' => 'annonces.contact.spam_field_label'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\Contact',
			'spam_protection' => true,
			'spam_protection_time' => 10,
		));
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix()
	{
		return 'contact';
	}
}