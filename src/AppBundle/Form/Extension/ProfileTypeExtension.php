<?php 
namespace AppBundle\Form\Extension;

use AppBundle\Entity\User;
use AppBundle\Form\AddressType;
use FOS\UserBundle\Form\Type\ProfileFormType;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

final class ProfileTypeExtension extends AbstractTypeExtension
{
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// Adding new fields works just like in the parent form type.
		$builder->add('nom',  TextType::class, array('label' => 'annonces.form.nom_label', 'required' => false))
			->add('prenom', TextType::class, array('label' => 'annonces.form.prenom_label', 'required' => false))
			->add('adresse', AddressType::class, array('label' => false))
			->add('tel', TextType::class, array('label' => 'annonces.form.tel_label', 'required' => false));

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
			$user = $event->getData();
			$form = $event->getForm();

			// checks if the Product object is "new"
			// If no data is passed to the form, the data is "null".
			// This should be considered a new "Product"
			if ($user != null && $user->getType() == 'pro')
			{
				$form->add('nomPro',  TextType::class, array('label' => 'annonces.form.pro.nom', 'required' => false))
					->add('siren',  TextType::class, array('required' => false))
					->add('descriptionPro',  TextareaType::class, array('label' => 'annonces.form.pro.description', 'required' => false))
					->add('annuairePro',  CheckboxType::class, array('label' => 'annonces.form.pro.annuaire', 'required' => false))
					->remove("nom")
					->remove("prenom");
			}
		});
	}

	/**
	 * {@inheritdoc}
	 */
	public function getExtendedType()
	{
		return ProfileFormType::class;
	}
	
}