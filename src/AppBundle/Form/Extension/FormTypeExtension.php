<?php 
namespace AppBundle\Form\Extension;

use AppBundle\Validator\Constraints\AntiSpamConstraint;
use Monolog\Logger;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class FormTypeExtension extends AbstractTypeExtension
{
	private $logger;

	public function __construct(Logger $logger)
	{
		$this->logger = $logger;
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		if ($options["spam_protection"] === true)
		{
			// Adding new fields works just like in the parent form type.
			$builder->add('_time_token', HiddenType::class, [
				'data' => time(),
				'constraints' => [new AntiSpamConstraint($options["spam_protection_time"])],
				'mapped' => false
			]);
		}
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'spam_protection' => false,
			'spam_protection_time' => 10
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getExtendedType()
	{
		return FormType::class;
	}
	
}