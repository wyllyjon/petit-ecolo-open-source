<?php 
namespace AppBundle\Form\Extension;

use AppBundle\Entity\User;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class RegistrationTypeExtension extends AbstractTypeExtension
{
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// Adding new fields works just like in the parent form type.
		$builder->add('type', ChoiceType::class, [
			'label' => 'security.registration.type_label',
			'choices' => array('Particulier' => User::$TYPE_PARTICULIER, 'Professionnel' => User::$TYPE_PRO),
			'expanded' => true,
			'choice_attr' => function(){
				return array('onclick' => 'togglePro(this)');
			},
			//'choice_attr' => array('class' => 'form-control', 'onclick' => 'togglePro(this)')
		]);
		$builder->add('descriptionPro', TextareaType::class, array('label' => 'annonces.form.pro.description', "required" => false))
			->add("siren", TextType::class, array("required" => false))
			->add("nomPro", TextType::class, array('label' => 'annonces.form.pro.nom', "required" => false))
			->add("annuairePro", CheckboxType::class, array('label' => 'annonces.form.pro.annuaire', "required" => false));
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'spam_protection' => true,
			'spam_protection_time' => 7,
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getExtendedType()
	{
		return RegistrationFormType::class;
	}
	
}