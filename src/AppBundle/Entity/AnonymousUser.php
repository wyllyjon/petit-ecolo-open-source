<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AnonymousUser
 *
 * @ORM\Table(name="anonymous_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnonymousUserRepository")
 */
class AnonymousUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(name="newsletter", type="boolean", nullable=true)
     */
    private $newsletter;

	/**
	 * @ORM\OneToOne(targetEntity="Address", cascade={"persist"})
	 * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
	 *
	 * @Assert\Valid
	 */
	private $adresse;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return mixed
	 */
	public function getAdresse()
	{
		return $this->adresse;
	}

	/**
	 * @param mixed $adresse
	 */
	public function setAdresse($adresse)
	{
		$this->adresse = $adresse;
	}



    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return AnonymousUser
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return AnonymousUser
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }


    /**
     * Set email
     *
     * @param string $email
     *
     * @return AnonymousUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     *
     * @return AnonymousUser
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return bool
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

	public function __toString()
	{
		return $this->getNom()." / ".$this->getEmail();
	}
}

