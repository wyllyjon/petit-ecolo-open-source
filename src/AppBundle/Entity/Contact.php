<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;


class Contact
{
	public static $SUJET_PUB = "pub";
	public static $SUJECT_BUG = "bug";
	public static $SUJECT_SIGNALER = "signaler";
	public static $SUJECT_AMELIORATION = "amelioration";
	public static $SUJECT_AUTRE = "autre";


	// Champ ne correspondant à rien, ne devant pas être rempli, mais à destination des bots
	// Si ce champ est rempli (alors qu'on va le cacher), c'est que c'est un robot qui a rempli le formulaire

	private $spsd;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $mail;

	/**
	 * @Assert\NotBlank()
	 */
	private $nom;

	/**
	 * @Assert\NotBlank()
	 */
	private $sujet;

	/**
	 * @Assert\NotBlank()
	 */
	private $message;


	/**
	 * @return mixed
	 */
	public function getMail()
	{
		return $this->mail;
	}

	/**
	 * @param mixed $mail
	 */
	public function setMail($mail)
	{
		$this->mail = $mail;
	}

	/**
	 * @return mixed
	 */
	public function getNom()
	{
		return $this->nom;
	}

	/**
	 * @param mixed $nom
	 */
	public function setNom($nom)
	{
		$this->nom = $nom;
	}

	/**
	 * @return mixed
	 */
	public function getSujet()
	{
		return $this->sujet;
	}

	/**
	 * @param mixed $sujet
	 */
	public function setSujet($sujet)
	{
		$this->sujet = $sujet;
	}

	/**
	 * @return mixed
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param mixed $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @return mixed
	 */
	public function getSpsd()
	{
		return $this->spsd;
	}

	/**
	 * @param mixed $spsd
	 */
	public function setSpsd($spsd): void
	{
		$this->spsd = $spsd;
	}


}