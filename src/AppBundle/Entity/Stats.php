<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stats
 *
 * @ORM\Table(name="stats")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatsRepository")
 */
class Stats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_annonces", type="integer", nullable=false)
     */
    private $nbAnnonces = 0 ;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_utilisateurs", type="integer", nullable=false)
     */
    private $nbUtilisateurs= 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbAnnonces
     *
     * @param integer $nbAnnonces
     *
     * @return Stats
     */
    public function setNbAnnonces($nbAnnonces)
    {
        $this->nbAnnonces = $nbAnnonces;

        return $this;
    }

    /**
     * Get nbAnnonces
     *
     * @return int
     */
    public function getNbAnnonces()
    {
        return $this->nbAnnonces;
    }

    /**
     * Set nbUtilisateurs
     *
     * @param integer $nbUtilisateurs
     *
     * @return Stats
     */
    public function setNbUtilisateurs($nbUtilisateurs)
    {
        $this->nbUtilisateurs = $nbUtilisateurs;

        return $this;
    }

    /**
     * Get nbUtilisateurs
     *
     * @return int
     */
    public function getNbUtilisateurs()
    {
        return $this->nbUtilisateurs;
    }
}

