<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{

	public static $TYPE_PRO = 'pro';
	public static $TYPE_PARTICULIER = 'particulier';

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=20, nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="civility", type="string", length=5, nullable=true)
     */
    private $civility;

    /**
     * @var bool
     *
     * @ORM\Column(name="newsletter", type="boolean", nullable=true)
     */
    private $newsletter;

	/**
	 * @ORM\OneToMany(targetEntity="AnnoncesBundle\Entity\Annonce", mappedBy="user", cascade={"remove"})
	 */
    private $annonces;


	/**
     * @ORM\OneToOne(targetEntity="Address", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
	 *
	 * @Assert\Valid
     */
    private $adresse;

	/**
	 * @var string
	 * Pro / particulier
	 *
	 * @ORM\Column(name="type", type="string")
	 */
    private $type;

    // PREFERENCES

	/**
	 * @ORM\ManyToMany(targetEntity="AnnoncesBundle\Entity\Annonce", inversedBy="usersFavoris", cascade={"remove"})
	 * @ORM\JoinTable(name="users_annoncesFavorites")
	 */
	private $annoncesFavorites;

	/**
	 * @ORM\ManyToMany(targetEntity="AnnoncesBundle\Entity\Annonce", inversedBy="usersSignales")
	 * @ORM\JoinTable(name="users_annoncesSignalees")
	 */
	private $annoncesSignalees;

	// POUR LES PROS

	/**
	 * @ORM\Column(name="nom_pro", type="text", nullable=true)
	 *
	 */
	private $nomPro;

	/**
	 *
	 * @ORM\Column(name="description_pro", type="text", nullable=true)
	 *
	 */
	private $descriptionPro;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="siren", type="string", nullable=true)
	 */
	private $siren;

	/**
	 * @var boolean
	 * Si le professionnel est visible sur l'annuaire des pros
	 *
	 * @ORM\Column(name="annuaire_pro", type="boolean", nullable=true)
	 */
	private $annuairePro;

	public function __construct()
	{
		$this->annoncesFavorites = new ArrayCollection();
		parent::__construct();
	}

	/**
	 * @return mixed
	 */
	public function getNomPro()
	{
		return $this->nomPro;
	}

	/**
	 * @param mixed $nomPro
	 */
	public function setNomPro($nomPro): void
	{
		$this->nomPro = $nomPro;
	}



	/**
	 * @return mixed
	 */
	public function getDescriptionPro()
	{
		return $this->descriptionPro;
	}

	/**
	 * @param mixed $descriptionPro
	 */
	public function setDescriptionPro($descriptionPro)
	{
		$this->descriptionPro = $descriptionPro;
	}

	/**
	 * @return bool
	 */
	public function isAnnuairePro()
	{
		return $this->annuairePro;
	}

	/**
	 * @param bool $annuairePro
	 */
	public function setAnnuairePro(bool $annuairePro)
	{
		$this->annuairePro = $annuairePro;
	}

	/**
	 * @return string
	 */
	public function getSiren()
	{
		return $this->siren;
	}

	/**
	 * @param string $siren
	 */
	public function setSiren(string $siren)
	{
		$this->siren = $siren;
	}


	/**
	 * @return mixed
	 */
	public function getAnnoncesFavorites()
	{
		return $this->annoncesFavorites;
	}

	/**
	 * @param mixed $annoncesFavorites
	 */
	public function setAnnoncesFavorites($annoncesFavorites)
	{
		$this->annoncesFavorites = $annoncesFavorites;
	}

	/**
	 * @param $annonceFavorite
	 */
	public function addAnnonceFavorite($annonceFavorite)
	{
		if ($this->annoncesFavorites->contains($annonceFavorite)) {
			return;
		}
		$this->annoncesFavorites->add($annonceFavorite);
	}
	/**
	 * @param $annonceFavorite
	 */
	public function removeAnnonceFavorite($annonceFavorite)
	{
		if (!$this->annoncesFavorites->contains($annonceFavorite)) {
			return;
		}
		$this->annoncesFavorites->removeElement($annonceFavorite);
	}

	/**
	 * @return mixed
	 */
	public function getAnnoncesSignalees()
	{
		return $this->annoncesSignalees;
	}

	/**
	 * @param mixed $annoncesSignalees
	 */
	public function setAnnoncesSignalees($annoncesSignalees)
	{
		$this->annoncesSignalees = $annoncesSignalees;
	}

	/**
	 * @param $annoncesSignalees
	 */
	public function addAnnonceSignalee($annonceSignalee)
	{
		if ($this->annoncesSignalees->contains($annonceSignalee)) {
			return;
		}
		$this->annoncesSignalees->add($annonceSignalee);
	}
	/**
	 * @param $annoncesSignalees
	 */
	public function removeAnnonceSignalee($annonceSignalee)
	{
		if (!$this->annoncesSignalees->contains($annonceSignalee)) {
			return;
		}
		$this->annoncesSignalees->removeElement($annonceSignalee);
	}





	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}




    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setNom($name)
    {
        $this->nom = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set firstname
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set civility
     *
     * @param string $civility
     *
     * @return User
     */
    public function setCivility($civility)
    {
        $this->civility = $civility;

        return $this;
    }

    /**
     * Get civility
     *
     * @return string
     */
    public function getCivility()
    {
        return $this->civility;
    }

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     *
     * @return User
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return bool
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set annonces
     *
     * @param \stdClass $annonces
     *
     * @return User
     */
    public function setAnnonces($annonces)
    {
        $this->annonces = $annonces;

        return $this;
    }

    /**
     * Get annonces
     *
     * @return \stdClass
     */
    public function getAnnonces()
    {
        return $this->annonces;
    }

    /**
     * Set adresse
     *
     * @param Address $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return Address
     */
    public function getAdresse()
    {
        return $this->adresse;
    }
}

