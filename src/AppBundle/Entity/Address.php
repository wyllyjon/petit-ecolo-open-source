<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 */
class Address
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=500, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postal", type="string", length=10)
     */
    private $postal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="region", type="string", length=255, nullable=true)
	 */
	private $region;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="longitude", type="float", length=500)
	 *
	 */
    private $lng;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="latitude", type="float", length=500)
	 *
	 */
    private $lat;

	/**
	 * @Assert\Callback
	 */
	public function validate(ExecutionContextInterface $context, $payload)
	{
		if ($this->lng == null || $this->lat == null)
		{
			$context->buildViolation('annonces.form.errors.lat_lng')
				->addViolation();
		}
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postal
     *
     * @param string $postal
     *
     * @return Address
     */
    public function setPostal($postal)
    {
        $this->postal = $postal;



        return $this;
    }

    /**
     * Get postal
     *
     * @return string
     */
    public function getPostal()
    {
        return $this->postal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Address
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

	/**
	 * @return string
	 */
	public function getRegion()
	{
		return $this->region;
	}

	/**
	 * @param string $region
	 */
	public function setRegion(string $region)
	{
		$this->region = $region;
	}



	/**
	 * @return string
	 */
	public function getLng()
	{
		return $this->lng;
	}

	/**
	 * @param string $long
	 */
	public function setLng($long)
	{
		$this->lng = $long;
	}

	/**
	 * @return string
	 */
	public function getLat()
	{
		return $this->lat;
	}

	/**
	 * @param string $lat
	 */
	public function setLat($lat)
	{
		$this->lat = $lat;
	}

}

