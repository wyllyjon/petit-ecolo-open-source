<?php

namespace AppBundle\DataFixtures\ORM;

use AnnoncesBundle\Entity\Annonce;
use AnnoncesBundle\Entity\Categorie;
use AppBundle\Entity\Address;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		$this->loadUsers($manager);
		$this->loadCategories($manager);
		$this->loadAnnonces($manager);
	}

	public function loadUsers(ObjectManager $manager)
	{
		for ($i = 0; $i < 10; $i++) {
			$adresse = new Address();
			$adresse->setAddress("$i place de la libération");
			$adresse->setPostal("6400".$i);
			$adresse->setVille("Lescar_$i");
			$adresse->setLng(rand(-500000, 1000000) / 100000);
			$adresse->setLat(rand(4300000,5100000)/100000);

			$user = new User();
			$user->setNom("Wendlinger_".$i);
			$user->setPrenom("Jonathan_".$i);
			$user->setAdresse($adresse);
			$user->setCivility("M.");
			$user->setEmail("jowe31+$i@gmail.com");
			$hash = $this->container->get('security.password_encoder')->encodePassword($user, 'test');
			$user->setPassword($hash);
			$user->setEnabled(true);
			$user->setUsername("wyllyjon$i");

			$type = rand(0,1);
			$user->setType($type == 1 ? User::$TYPE_PRO : User::$TYPE_PARTICULIER);

			$manager->persist($user);

			$this->addReference('user'.$i, $user);
		}

		$admin = new User();
		$admin->setNom("Admin");
		$admin->setPrenom("Jonathan");
		$admin->setEmail("jowe31+admin@gmail.com");
		$hash = $this->container->get('security.password_encoder')->encodePassword($admin, 'admin');
		$admin->setPassword($hash);
		$admin->setUsername("admin");
		$admin->setEnabled(true);
		$admin->setRoles(array('ROLE_ADMIN'));
		$admin->setType(User::$TYPE_PARTICULIER);
		$manager->persist($admin);

		$manager->flush();
	}

	public function loadCategories(ObjectManager $manager)
	{
		$parent = new Categorie();
		$nom = "Enfants";
		$parent->setNom($nom);
		$parent->setCode("enfants");
		$parent->setPositionAffichage(0);
		$manager->persist($parent);
		$this->addReference('categorie_0', $parent);

			$categorie = new Categorie();
			$nom = "Vêtements et accessoires";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_vetements");
			$categorie->setPositionAffichage(0);
			$categorie->setParent($parent);
			$manager->persist($categorie);
			$this->addReference('categorie_5', $categorie);

			$categorie = new Categorie();
			$nom = "Jeux et loisirs";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_jeux");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(1);
			$manager->persist($categorie);
			$this->addReference('categorie_6', $categorie);

			$categorie = new Categorie();
			$nom = "Transport et portage";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_transport");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(2);
			$manager->persist($categorie);
			$this->addReference('categorie_7', $categorie);

			$categorie = new Categorie();
			$nom = "Livres";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_livres");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(3);
			$manager->persist($categorie);
			$this->addReference('categorie_8', $categorie);

			$categorie = new Categorie();
			$nom = "Chambre et mobilier";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_mobilier");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(4);
			$manager->persist($categorie);
			$this->addReference('categorie_9', $categorie);

			$categorie = new Categorie();
			$nom = "Alimentation";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_alimentation");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(5);
			$manager->persist($categorie);
			$this->addReference('categorie_10', $categorie);

			$categorie = new Categorie();
			$nom = "Allaitement";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_allaitement");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(6);
			$manager->persist($categorie);
			$this->addReference('categorie_11', $categorie);

			$categorie = new Categorie();
			$nom = "Hygiène et soins";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_hygiene");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(7);
			$manager->persist($categorie);
			$this->addReference('categorie_12', $categorie);

			$categorie = new Categorie();
			$nom = "Santé et bien-être";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_sante");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(7);
			$manager->persist($categorie);
			$this->addReference('categorie_23', $categorie);

			$categorie = new Categorie();
			$nom = "Sécurité";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_securite");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(8);
			$manager->persist($categorie);
			$this->addReference('categorie_13', $categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(9);
			$manager->persist($categorie);
			$this->addReference('categorie_14', $categorie);


		$parent = new Categorie();
		$nom = "Jardin";
		$parent->setNom($nom);
		$parent->setCode("jardin");
		$parent->setPositionAffichage(1);
		$manager->persist($parent);

			$categorie = new Categorie();
			$nom = "Semences";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_semences");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Outils";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_outils");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Partage de terrain";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_terrain");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(2);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);


		$parent = new Categorie();
		$nom = "Véhicules";
		$parent->setNom($nom);
		$parent->setCode("vehicules");
		$parent->setPositionAffichage(6);
		$manager->persist($parent);
		$this->addReference('categorie_1', $parent);

			$categorie = new Categorie();
			$nom = "Electriques";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_electriques");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(0);
			$manager->persist($categorie);
			$this->addReference('categorie_19', $categorie);

			$categorie = new Categorie();
			$nom = "Hybrides (essence ou diesel)";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_hybrides");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(1);
			$manager->persist($categorie);
			$this->addReference('categorie_20', $categorie);


			$categorie = new Categorie();
			$nom = "Autres énergies";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_autresEnergies");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(2);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Locations";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_locations");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(3);
			$manager->persist($categorie);
			$this->addReference('categorie_21', $categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(9);
			$manager->persist($categorie);
			$this->addReference('categorie_22', $categorie);


		$parent = new Categorie();
		$nom = "Maison";
		$parent->setNom($nom);
		$parent->setCode("maison");
		$parent->setPositionAffichage(7);
		$manager->persist($parent);
		$this->addReference('categorie_3', $parent);

			$categorie = new Categorie();
			$nom = "Meubles";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_meubles");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(0);
			$manager->persist($categorie);
			$this->addReference('categorie_16', $categorie);

			$categorie = new Categorie();
			$nom = "Eléctroménager";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_electromenager");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(1);
			$manager->persist($categorie);
			$this->addReference('categorie_17', $categorie);

			$categorie = new Categorie();
			$nom = "Linge de maison";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_linge");
			$categorie->setParent($parent);
		$categorie->setPositionAffichage(2);
			$manager->persist($categorie);
			$this->addReference('categorie_15', $categorie);


			$categorie = new Categorie();
			$nom = "Vêtements et accessoires";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_vetements");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(3);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(4);
			$manager->persist($categorie);
			$this->addReference('categorie_18', $categorie);

		$parent = new Categorie();
		$nom = "Emplois / stages / Formations";
		$parent->setNom($nom);
		$parent->setCode("emploi");
		$parent->setPositionAffichage(4);
		$manager->persist($parent);
		$this->addReference('categorie_2', $parent);

			$categorie = new Categorie();
			$nom = "Offre d'emploi";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_emploi");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Offre de stage";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_stage");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Formation";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_formation");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(2);
			$manager->persist($categorie);


			$categorie = new Categorie();
			$nom = "Formation Professionnalisante";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_formationPro");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(3);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);


		$parent = new Categorie();
		$nom = "Immobilier";
		$parent->setNom($nom);
		$parent->setCode("immobilier");
		$parent->setPositionAffichage(5);
		$manager->persist($parent);

			$categorie = new Categorie();
			$nom = "Vente";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_vente");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Location";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_location");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Collocation";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_colocation");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(2);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Location de vacances";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_vacances");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(3);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);

		$parent = new Categorie();
		$nom = "Electronique";
		$parent->setNom($nom);
		$parent->setCode("electronique");
		$parent->setPositionAffichage(8);
		$manager->persist($parent);

			$categorie = new Categorie();
			$nom = "Téléphones";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_telephone");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Ordinateurs";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_ordinateurs");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);


		$parent = new Categorie();
		$nom = "Loisirs";
		$parent->setNom($nom);
		$parent->setCode("loisirs");
		$parent->setPositionAffichage(9);
		$manager->persist($parent);

			$categorie = new Categorie();
			$nom = "Sports";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_sport");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Musique";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_musiques");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Jeux";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_jeux");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(2);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);


		$parent = new Categorie();
		$nom = "Services";
		$parent->setNom($nom);
		$parent->setCode("services");
		$parent->setPositionAffichage(3);
		$manager->persist($parent);

			$categorie = new Categorie();
			$nom = "Covoiturage";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_covoiturage");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Aide à la personne";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_aidePersonne");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Service à domicile";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_domicile");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(2);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Réparation et rénovation";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_reparation");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(3);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);

		$parent = new Categorie();
		$nom = "Art et artisanat";
		$parent->setNom($nom);
		$parent->setCode("art");
		$parent->setPositionAffichage(2);
		$manager->persist($parent);

			$categorie = new Categorie();
			$nom = "Peinture";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_peinture");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Sculpture";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_sculpture");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Vêtements";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_vetements");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(2);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Bijoux et accessoires";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_bijoux");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(3);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Nourriture et boissons";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_nourriture");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(4);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);

		$parent = new Categorie();
		$nom = "Animaux";
		$parent->setNom($nom);
		$parent->setCode("animaux");
		$parent->setPositionAffichage(10);
		$manager->persist($parent);

			$categorie = new Categorie();
			$nom = "Adoption";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_adoption");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(0);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Services";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_services");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(1);
			$manager->persist($categorie);

			$categorie = new Categorie();
			$nom = "Divers";
			$categorie->setNom($nom);
			$categorie->setCode($parent->getCode()."_divers");
			$categorie->setParent($parent);
			$categorie->setPositionAffichage(9);
			$manager->persist($categorie);


		$parent = new Categorie();
		$nom = "Divers";
		$parent->setNom($nom);
		$parent->setCode("divers");
		$parent->setPositionAffichage(99);
		$manager->persist($parent);
		$this->addReference('categorie_4', $parent);

		$manager->flush();
	}

	public function loadAnnonces(ObjectManager $manager)
	{
		for ($i = 0; $i < 200; $i++) {
			$annonce = new Annonce();
				$annonce->setUser($this->getReference('user'. rand(0,9)));
			$annonce->setCategorie($this->getReference('categorie_'. rand(0,14)));
			$annonce->setComplexId(uniqid("$i"));
			$monnaie = rand(0,1);
			$annonce->setMonnaie($monnaie ? "euro" : "g1");
			$annonce->setMonnaiePaiement($monnaie ? "euro" : "g1");

			if(rand(0,20) == 1)
			{
				$date = new \DateTime();
				$annonce->setDateAlaune($date);
				$dateFin = new \DateTime();
				$annonce->setDateFinAlaune($dateFin->add(new \DateInterval('P10D'))); // P10D : Plus 10 days
			}

			$date = new \DateTime();
			$date->setDate(rand(2015,2017), rand(1,12), rand(1,25));
			$annonce->setDateCreation($date);
			$annonce->setDateModification($date);

			$annonce->setPrix(rand(1,2580));

			$annonce->setTexte("Texte de l'annonce $i, ça sera toujours le même, tant pis...");
			$annonce->setTitre("Titre de l'annonce $i");

			$offre = rand(0,1);
			$annonce->setType($offre == 1 ? 'offre' : "demande");

			$tel = rand(0,1);
			$annonce->setShowTel($tel == 1 ? true : false);


			$manager->persist($annonce);
		}

		$manager->flush();
	}
}