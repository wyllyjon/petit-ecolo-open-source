<?php

namespace AppBundle\Services;

class UrlHelper
{
	public function urlEscape($url)
	{
		$url = preg_replace('#Ç|ç#', 'c', $url);
		$url = preg_replace('#è|é|ê|ë|È|É|Ê|Ë#', 'e', $url);
		$url = preg_replace('#à|á|â|ã|ä|å|@|À|Á|Â|Ã|Ä|Å#', 'a', $url);
		$url = preg_replace('#ì|í|î|ï|Ì|Í|Î|Ï#', 'i', $url);
		$url = preg_replace('#ð|ò|ó|ô|õ|ö|Ò|Ó|Ô|Õ|Ö#', 'o', $url);
		$url = preg_replace('#ù|ú|û|ü|Ù|Ú|Û|Ü#', 'u', $url);
		$url = preg_replace('#ý|ÿ|Ý#', 'y', $url);
		$url = preg_replace('# |\'#', '_', $url);
		$url = strtr($url, '/', '_');

		return ($url);
		//return strtr($url,' àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', '_aaaaaceeeeiiiinooooouuuuyyaaaaaceeeeiiiinooooouuuuy');
	}
}