<?php

namespace AppBundle\Services;


use AnnoncesBundle\Entity\Alerte;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;

class MailSender
{
	private $mailer;
	private $templating;
	private $translator;
	private $container;
	private $logger;

	public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, ContainerInterface $container, Logger $logger, Translator $translator)
	{
		$this->mailer = $mailer;
		$this->templating = $templating;
		$this->container = $container;
		$this->logger = $logger;
		$this->translator = $translator;
	}

	public function sendContactAnnonce($nom, $mail, $message, $destinataire, $annonce)
	{

		$result = $this->sendMessage(
			$destinataire->getEmail(),
			$this->container->getParameter('admin_mail'),
			$mail,
			$this->translator->trans('annonces.contact.sujet_courriel'),
			$this->templating->render(
				'emails/contact.html.twig',
				array('nom_destinataire' => $destinataire->getNom(), 'nom_expediteur' => $nom, 'message' => $message, 'annonce' => $annonce)
			),
			$this->templating->render(
				'emails/contact.txt.twig',
				array('nom_destinataire' => $destinataire->getNom(), 'nom_expediteur' => $nom, 'message' => $message, 'annonce' => $annonce)
			)
		);

		if (!$result)
		{
			$this->logger->error("Une erreur est survenue lors de l'envoi du mail de contact (via une annonce) : ". print_r($result));
			return false;

		}
		return true;
	}

	public function sendGeneralContact($user_mail, $user_nom, $user_message, $sujet, $destinataire, $template)
	{
		$result = $this->sendMessage($destinataire, 'contact@petitecolo.fr', $user_mail, '[Petit Ecolo.fr] '.$sujet,
			$this->templating->render(
				$template,
				array('mail' => $user_mail, 'nom' =>$user_nom, 'message' => $user_message)
			)
		);

		if (!$result)
		{
			$this->logger->error("Une erreur est survenue lors de l'envoi du mail de contact (via une annonce) : ". print_r($result));
			return false;

		}
		return true;
	}

	public function sendAnnonceAttenteModeration($nom, $mail)
	{
		$result_client = $this->sendMessage(
			$mail,
			$this->container->getParameter('admin_mail'),
			$this->container->getParameter('noreply_mail'),
			$this->translator->trans('annonces.contact.sujet_annonce_attente_moderation'),
			$this->templating->render(
				'emails/attente_moderation.html.twig',
				array('nom' => $nom)
			),
			$this->templating->render(
				'emails/attente_moderation.txt.twig',
				array('nom' => $nom)
			)
		);

		$result_admin = $this->sendMessage(
			$this->container->getParameter('admin_mail'),
			'appli@petitecolo.fr',
			$this->container->getParameter('noreply_mail'),
			$this->translator->trans('annonces.contact.sujet_annonce_attente_moderation_admin'),
			$this->templating->render(
				'emails/attente_moderation_admin.html.twig'
			)
		);

		if (!$result_admin)
		{
			$this->logger->error("Une erreur est survenue lors de l'envoi du mail d'information à l'admin qu'une annonce est en attente de modération) : ".print_r($result_admin));
		}


		if (!$result_client)
		{
			$this->logger->error("Une erreur est survenue lors de l'envoi du mail de confirmation de création d'annonce (annonce en attente de modération) : " .print_r($result_client));
			return false;
		}

		return true;
	}

	public function sendAnnonceNonConfirmee($nom, $mail, $id, $complexId)
	{
		$result_client = $this->sendMessage(
			$mail,
			$this->container->getParameter('admin_mail'),
			$this->container->getParameter('noreply_mail'),
			$this->translator->trans('annonces.contact.sujet_annonce_non_confirmee'),
			$this->templating->render(
				'emails/non_confirmee.html.twig',
				array('nom' => $nom, 'id' => $id, 'complexId' => $complexId)
			),
			$this->templating->render(
				'emails/non_confirmee.txt.twig',
				array('nom' => $nom, 'id' => $id, 'complexId' => $complexId)
			)
		);

		if (!$result_client)
		{
			$this->logger->error("Une erreur est survenue lors de l'envoi du mail d'information sur le fait que l'annonce n'a pas été confirmée : " .print_r($result_client));
			return false;
		}

		return true;
	}

	public function annonceValidee($user, $annonce)
	{
		$mail = $user->getEmail();
		$nom = $user->getNom();

		$result_client = $this->sendMessage(
			$mail,
			$this->container->getParameter('admin_mail'),
			$this->container->getParameter('noreply_mail'),
			$this->translator->trans('annonces.contact.sujet_annonce_validee'),
			$this->templating->render(
				'emails/annonce_validee.html.twig',
				array('nom' => $nom, 'titre' => $annonce->getTitre(), 'complexId' => $annonce->getComplexId(), 'id' => $annonce->getId())
			),
			$this->templating->render(
				'emails/annonce_validee.txt.twig',
				array('nom' => $nom, 'titre' => $annonce->getTitre(), 'complexId' => $annonce->getComplexId(), 'id' => $annonce->getId())
			)
		);
	}

	public function annonceRefusee($annonce, $raison)
	{

		if ($annonce->getAnonymousUser())
		{
			$user = $annonce->getAnonymousUser();
		}
		else
		{
			$user = $annonce->getUser();
		}

		$mail = $user->getEmail();
		$nom = $user->getNom();

		$result_client = $this->sendMessage(
			$mail,
			$this->container->getParameter('admin_mail'),
			$this->container->getParameter('admin_mail'),
			$this->translator->trans('annonces.contact.sujet_annonce_refusee'),
			$this->templating->render(
				'emails/annonce_refusee.html.twig',
				array('id' => $annonce->getId(), 'complexId' => $annonce->getComplexId(), 'nom' => $nom, 'raison' => $raison, 'titre' => $annonce->getTitre())
			),
			$this->templating->render(
				'emails/annonce_refusee.txt.twig',
				array('id' => $annonce->getId(), 'complexId' => $annonce->getComplexId(), 'nom' => $nom, 'raison' => $raison, 'titre' => $annonce->getTitre())
			)
		);
	}

	public function annonceInvalidee($raison, $user, $annonce)
	{
		$mail = $user->getEmail();
		$nom = $user->getNom();

		$result_client = $this->sendMessage(
			$mail,
			$this->container->getParameter('admin_mail'),
			$this->container->getParameter('admin_mail'),
			$this->translator->trans('annonces.contact.sujet_annonce_devalidee'),
			$this->templating->render(
				'emails/annonce_invalidee.html.twig',
				array('nom' => $nom, 'raison' => $raison, 'annonce' => $annonce, 'titre' => $annonce->getTitre(), 'complexId' => $annonce->getComplexId(), 'id' => $annonce->getId())
			),
			$this->templating->render(
				'emails/annonce_invalidee.txt.twig',
				array('nom' => $nom, 'raison' => $raison, 'annonce' => $annonce, 'titre' => $annonce->getTitre(), 'complexId' => $annonce->getComplexId(), 'id' => $annonce->getId())
			)
		);
	}

	public function sendAlertes($user, $annonces, $titreAlerte, $rechercheUrl)
	{
		$result_client = $this->sendMessage(
			$user->getEmail(),
			$this->container->getParameter('admin_mail'),
			$this->container->getParameter('noreply_mail'),
			$this->translator->trans('annonces.list.alertes.titre_mail'),
			$this->templating->render(
				'emails/annonces_alerte.html.twig',
				array('annonces' => $annonces, 'titreAlerte' => $titreAlerte, 'rechercheUrl' => $rechercheUrl)
			)
//			$this->templating->render(
//				'emails/annonce_refusee.txt.twig',
//				array('nom' => $nom, 'raison' => $raison, 'titre' => $titreAnnonce)
//			)
		);
	}

	private function sendMessage($to, $from, $replyTo, $sujet, $bodyHtml, $bodyText = null)
	{
		$message = (new \Swift_Message($sujet))
			->setFrom($from)
			->setTo($to)
			->setReplyTo($replyTo)
			->setBody(
				$bodyHtml,
				'text/html'
			);
		if ($bodyText != null)
		{
			$message->addPart(
				$bodyText,
				'text/plain'
			);
		}

		$result = $this->mailer->send($message, $failures);
		if (!$result)
		{
			return $failures;
		}

		return true;
	}
}