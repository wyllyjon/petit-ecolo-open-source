<?php

namespace AppBundle\Services;


use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class ImagesHelper
{
	private $translator;
	private $container;
	private $logger;

	public function __construct(ContainerInterface $container, Logger $logger, Translator $translator)
	{
		$this->container = $container;
		$this->logger = $logger;
		$this->translator = $translator;
	}

	public function movePhoto($annonceId, $path)
	{
		$upload_dir = $this->container->getParameter('ajax_upload.upload_folder') . '/';
		$length = strlen($upload_dir);
		// Si l'image a déjà été chargée et bougée (modification d'annonce), elle aura l'upload_dir au début de son path
		if (substr($path, 0, $length) === $upload_dir)
		{
			return $path;
		}


		if ($annonceId == null)
		{
			$this->logger->error("L'id de l'annonce n'a pas été fourni pour déplacer l'image");
		}
		$web_dir = $this->container->getParameter('kernel.root_dir') . '/../web/';
		$upload_path = $web_dir . $upload_dir;

		$fs = new Filesystem();

		// On créé le dossier parent (id de l'annonce)
		if(!file_exists($upload_path.$annonceId))
		{
			try {
				$fs->mkdir($upload_path.$annonceId);
			}
			catch (IOExceptionInterface $e) {
				$this->logger->error("Une erreur est survenue lors de la création du dossier pour bouger une image uploadée : ". $upload_path.$annonceId);
				return null;
			}
		}

		// On bouge la photo et on la retaille pour qu'elle ne soit pas trop grande
		$newPath = $upload_path.$annonceId."/".$path;
		$oldPath = $upload_path.$path;
		if (file_exists($oldPath))
		{
			$this->resizePhoto($oldPath);
			$fs->copy($oldPath, $newPath, true);
			$fs->remove($oldPath);
			return $upload_dir.$annonceId."/".$path;
		}
		else
		{
			$this->logger->error("Impossible de bouger une image après l'upload. Elle n'existe pas : ". $oldPath);
			return null;
		}
	}

	public function resizePhoto($path)
	{
		$imagine = new Imagine();

		try {
			$imageSize = getimagesize($path);
			// [0] : width / [1] : height
			if ($imageSize[0] > 750 || $imageSize[1] > 600)
			{
				$ratio = $imageSize[0] / $imageSize[1];

				// Taille de l'image
				if ($ratio <= 1)
				{
					// Si image verticale, on fixe la hauteur max à 600
					$size = new Box(600 * $ratio, 600);
				}
				else
				{
					// Si image horizontale, on fixe la largeur max à 750
					$size = new Box(750, 750 / $ratio);
				}

				$imagine->open($path)->resize($size)->save($path);
			}
		}
		catch (\Exception $e)
		{
			$this->logger->error("Impossible de récupérer la taille de l'image uploadée");
			throw new \Exception("Erreur lors du traitement de l'image");
		}
	}
}