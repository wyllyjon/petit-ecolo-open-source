<?php

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends AbstractAdmin
{
	 protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('edit');
        $collection->remove('create');
    }

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper->addIdentifier('username');
		$listMapper->addIdentifier('prenom');
		$listMapper->addIdentifier('nom');
		$listMapper->addIdentifier('email');
		$listMapper->addIdentifier('enabled');
	}


}