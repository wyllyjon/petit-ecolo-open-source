<?php

namespace AppBundle\Controller;

use AnnoncesBundle\Entity\Recherche;
use AppBundle\Entity\Contact;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
		$recherche = new Recherche();

		$form = $this->createForm('AnnoncesBundle\Form\RechercheType', $recherche);

		$em = $this->getDoctrine()->getManager();
		$repoCategorie = $em->getRepository('AnnoncesBundle:Categorie');
		$rootCategories = $repoCategorie->findRootCategories();

		$dernieres = $em->getRepository('AnnoncesBundle:Annonce')->findLast(12);

		foreach ($rootCategories as $rootCategory)
		{
			$rootCategory->setEnfants($repoCategorie->findByParent($rootCategory));
		}

//		$annoncesRepo = $em->getRepository('AnnoncesBundle:Annonce');
//		$annoncesAlaune = $annoncesRepo->findAllAlaune();

//		if (sizeof($annoncesAlaune) > 0)
//		{
//			foreach ($annoncesAlaune as $annonce)
//			{
//				$annonce->setDateDerniereMiseAlaune(new \DateTime());
//				$em->persist($annonce);
//			}
//			$em->flush();
//		}


        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
			'search_form' => $form->createView(),
			'dernieres_annonces' => $dernieres,
//			'annoncesAlaune' => $annoncesAlaune,
			'categories' => $rootCategories
        ]);
    }

	/**
	 * @Route("/page/{page}", name="page_statique")
	 */
	public function pageAction($page, Request $request)
	{
		// On limite quand même aux pages existantes
		$pagesDisponibles = array("mentions", "cgu", "soutenez-nous", "respect-donnees-personnelles", "fonctionnalites", "cookies", "aide", "pub");

		if (in_array($page, $pagesDisponibles))
		{
			$lang = $request->getLocale();
			try {
				// On essaie de trouver la page dans la langue du visiteur
				return $this->render('statiques/'.$lang.'/'.$page.'.html.twig');
  			} catch (\Exception $ex) {
				// Sinon on affiche la française.
				$this->get("logger")->warn("Aucune traduction de la page statique '$page' pour la langue '$lang'");
				return $this->render('statiques/fr/'.$page.'.html.twig');
			}
		}

		throw $this->createNotFoundException();
	}


	/**
	 * @Route("/contact/{type}", name="contact", defaults={"type" = null})
	 * @Method({"GET","POST"})
	 */
    public function contactAction(Request $request, \Swift_Mailer $mailer)
	{
		$contact = new Contact();
		$type = $request->get('type');
		if ($type != null)
		{
			$contact->setSujet($type);
		}
		$form = $this->createForm('AppBundle\Form\ContactType', $contact);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{
			$resultat = $this->sendMail($contact, $mailer);

			if ($resultat > 0)
			{
				return $this->render('contact/contact_success.html.twig', array(
					'form' => $form->createView(),
				));
			}
			else
			{
				return $this->render('contact/contact.html.twig', array(
					'form' => $form->createView(),
					'resultat' => 'ko'
				));
			}
		}

		return $this->render('contact/contact.html.twig', array(
			'form' => $form->createView()
		));
	}

	/**
	 * @Route("/renvoi-mail-inscription/{id}", name="resend-mail-inscription")
	 * @Method("POST")
	 */
	public function sendMailInscriptionAjaxAction(User $user)
	{
		$mailer = $this->get('fos_user.mailer');
		$mailer->sendConfirmationEmailMessage($user);

		return new JsonResponse(array('result' => 'ok'));
	}

	private function sendMail(Contact $contact, \Swift_Mailer $mailer)
	{
		if ($contact->getSpsd() != "")
		{
			$this->get("logger")->warn("Tentative de spam évité...");
			// Ce paramètre doit être vide (anti-spam)
			// On retourne true pour faire comme si le mail était bien parti.
			return true;
		}

		if ($contact->getSujet() == Contact::$SUJECT_BUG || $contact->getSujet() == Contact::$SUJECT_AMELIORATION)
		{
			$mailTo = array($this->container->getParameter('bugs_mail'), $this->container->getParameter('admin_email'));
			$template = "emails/contact_bug.html.twig";
		}
		elseif ($contact->getSujet() == Contact::$SUJET_PUB)
		{
			$mailTo = $this->container->getParameter('pub_mail');
			$template = "emails/contact_pub.html.twig";
		}
		else
		{
			$mailTo = $this->container->getParameter('admin_email');
			$template = "emails/contact_site.html.twig";
		}

		$mailSender = $this->container->get('AppBundle\Services\MailSender');

		return $mailSender->sendGeneralContact($contact->getMail(), $contact->getNom(), $contact->getMessage(), $contact->getSujet(), $mailTo, $template);
	}
}
