<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserProfileController extends Controller
{

	/**
	 *  @Route("/compte/supprimer", name="delete_account")
	 */
	public function deleteUserAction()
	{
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
		{
			throw new AccessDeniedException();
		}

		$user = $this->get('security.token_storage')->getToken()->getUser();

		$em = $this->getDoctrine()->getManager();
		$em->remove($user);

		$em->flush();

		return $this->redirectToRoute('homepage');
	}
}