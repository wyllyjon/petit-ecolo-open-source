<?php

namespace AppBundle\Repository;

/**
 * StatsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class StatsRepository extends \Doctrine\ORM\EntityRepository
{

	public function getStatNbUtilisateurs()
	{
		$queryString = 'SELECT s.nbUtilisateurs as nb FROM AppBundle\Entity\Stats s
						WHERE s.id=1';

		$query = $this->getEntityManager()
			->createQuery( $queryString);

		try {
			return $query->getSingleResult();
		}
		catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

	public function getStatNbAnnonces()
	{
		$queryString = 'SELECT s.nbAnnonces as nb FROM AppBundle\Entity\Stats s
						WHERE s.id=1';

		$query = $this->getEntityManager()
			->createQuery( $queryString);

		try {
			return $query->getSingleResult();
		}
		catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

}
