<?php


namespace AppBundle\Validator\Constraints;


use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AntiSpamConstraintValidator extends ConstraintValidator
{

	public function validate($value, Constraint $constraint)
	{
		if (!$constraint instanceof AntiSpamConstraint) {
			throw new UnexpectedTypeException($constraint, AntiSpamConstraint::class);
		}

		$dateEnd = time();

		// On teste si le formulaire a été soumis en un temps inférieur au temps minimum configuré dans les formulaires
		if ($dateEnd - $value < $constraint->getTime())
		{
			$this->context->buildViolation($constraint->message)->atPath('')->addViolation();
		}
	}
}

