<?php


namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class AntiSpamConstraint extends Constraint
{
	public $message = 'forms.antispam_validation_message';
	private $time;

	public function __construct($time)
	{
		parent::__construct(null);
		$this->time = $time;
	}

	public function getTime()
	{
		return $this->time;
	}


	public function validatedBy()
	{
		return \get_class($this).'Validator';
	}
}

