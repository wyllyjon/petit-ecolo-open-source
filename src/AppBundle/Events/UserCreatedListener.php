<?php

namespace AppBundle\Events;


use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCreatedListener implements EventSubscriberInterface
{
	private $em;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}

	public static function getSubscribedEvents()
	{
		return array(
			FOSUserEvents::REGISTRATION_CONFIRM => 'onUserCreated',
		);
	}

	public function onUserCreated(GetResponseUserEvent $event)
	{
		$stat = $this->em->getRepository("AppBundle:Stats")->find(1);
		$stat->setNbUtilisateurs($stat->getNbUtilisateurs() + 1);
		$this->em->flush();
	}
}