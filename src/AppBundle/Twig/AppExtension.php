<?php

namespace AppBundle\Twig;

use AnnoncesBundle\Entity\Annonce;
use AppBundle\Services\UrlHelper;
use Symfony\Component\Form\FormView;
use Symfony\Component\Security\Core\Exception\DisabledException;

class AppExtension extends \Twig_Extension
{
	private $helper;

	public function __construct(UrlHelper $helper)
	{
		$this->helper = $helper;
	}

	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('urlEscape', array($this, 'urlFilter')),
			new \Twig_SimpleFilter('hasMoreSearchValues', array($this, 'hasMoreSearchValues')),
			new \Twig_SimpleFilter('isDisabledException', array($this, 'isDisabledException')),
		);
	}

	public function getFunctions()
	{
		return array(
			new \Twig_SimpleFunction('isInstanceOf', array($this, 'isInstanceOf')),
			new \Twig_SimpleFunction('getClass', array($this, 'getClass')),
			new \Twig_SimpleFunction('startsWith', array($this, 'startsWith')),
			new \Twig_SimpleFunction('gettype', array($this, 'getType')),
		);
	}

	public function isInstanceOf($objet, $classe)
	{
		return $objet instanceof $classe;
	}

	public function getClass($objet)
	{
		return get_class($objet);
	}

	public function getType($objet)
	{
		return gettype($objet);
	}

	public function urlFilter($url)
	{
		return $this->helper->urlEscape($url);
	}

	public function startsWith($string, $value)
	{
		$length = strlen($value);
		return (substr($string, 0, $length) === $value);
	}

	/**
	 * Indique si des champs de la recherche avancée ont été renseignés ou non
	 * @param $searchForm
	 */
	public function hasMoreSearchValues(FormView $searchForm)
	{
		$children = $searchForm->children;

		if ($children["prixMin"]->vars['value'] !== ""
			|| $children["prixMax"]->vars['value'] !== ""
			|| $children["tailleEnfant"]->vars['value'] !== ""
			|| $children["taille"]->vars['value'] !== ""
			|| $children["typeContrat"]->vars['value'] !== ""
			|| $children["experienceMax"]->vars['value'] !== ""
			|| $children["tempsPartiel"]->vars['checked'] !== false
			|| $children["tempsPlein"]->vars['checked'] !== false
			|| $children["handiAcces"]->vars['checked'] !== false
			|| $children["remunerationMin"]->vars['value'] !== ""
			|| $children["dureeMin"]->vars['value'] !== ""
			|| $children["dureeMax"]->vars['value'] !== ""
			|| $children["typeBien"]->vars['value'] !== array()
			|| $children["surfaceMin"]->vars['value'] !== ""
			|| $children["surfaceMax"]->vars['value'] !== ""
			|| $children["nbPiecesMin"]->vars['value'] !== ""
			|| $children["nbPiecesMax"]->vars['value'] !== ""
			|| $children["meuble"]->vars['value'] !== ""
			|| $children["capacite"]->vars['value'] !== ""
			|| $children["marque"]->vars['value'] !== ""
			|| $children["modele"]->vars['value'] !== ""
			|| $children["anneeMin"]->vars['value'] !== ""
			|| $children["kilometrageMax"]->vars['value'] !== ""
			|| $children["boiteVitesse"]->vars['value'] !== ""
		){
			return true;
		}

		return false;
	}

	/**
	 * Indique si l'exception lors du login est une DisabledException
	 * @param exception $
	 */
	public function isDisabledException ($exception)
	{
		return $exception instanceof DisabledException;
	}
}